import "dotenv/config";
import { createParser } from "dashdash";
import { syncProfile } from "./bin/populate_mongo.js";
import { groups, args } from "./const/settings.js";
import {
  readProfile,
  detectSourceSetting,
  detectSocialSetting,
  detectFolderSetting,
  updateFolderSettings,
  updateS3Settings,
  updateDbxSettings,
  updateSocialSettings,
  profileIsValid,
  updateDbSettings,
  updateTweetSettings,
  updateIntervalSettings,
  fetchProfile,
  saveProfile,
  formatProfile,
  removeUndefined
} from "./lib/profile.js";
const options = [
  groups.profileOptions,
  args.name,
  args.formatProfile,
  args.workspace,
  args.validate,
  args.sync,
  args.download,
  args.init,
  groups.emptyOptions,
  args.debug,
  args.help,
];
(async function () {
  const parser = createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("profile.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true });
    console.log("usage: npm run profile [OPTIONS]\n" + "options:\n" + help);
    return;
  }

  if (!opts.name) {
    console.error("Profile name is missing '--name profilename'");
    return;
  }

  const location = opts.workspace;

  // try to find profile locally first
  var profile = await readProfile(location, opts.name);
  var foundProfile = Object.keys(profile).length > 1

  // if it doesn't exist then try to find it in the database
  if (!foundProfile) {
    console.warn(
      `Unable to find a profile with name "${opts.name}" locally, fetching from db ...`
    );
    let fetchedProfile = await fetchProfile(opts.name);
    if (!fetchedProfile) {
      if (!opts.init) {
        console.error(`Unable to locate profile with name "${opts.name}"`);
        return;
      } else {
        console.log('Initializing a new profile ...')
        await saveProfile(location, opts.name, profile);
      }
    } else {
      profile = fetchedProfile;
      delete profile["_id"];
      foundProfile = true;
    }
  }

  // save profile to local and return if download requested
  if (foundProfile && opts.download) {
    await saveProfile(location, opts.name, profile);
    console.log('saved profile to ' + location);
    return;
  }

  if (foundProfile && opts.formatProfile) {
    await formatProfile(location, opts.name, profile);
    console.log('cleared profile settings');
  }

  // expect to have a profile at this point
  // proceed to apply settings FROM .env TO profile

  const source = detectSourceSetting();
  if (!source) {
    console.error('Unable to detect "SOURCE" setting in .env');
    return;
  }

  if (source === "DBX") {
    await updateDbxSettings(location, profile);
  } else if (source === "S3") {
    await updateS3Settings(location, profile);
  } else {
    console.error(`Invalid "SOURCE": "${source}"`);
    return;
  }

  const socials = detectSocialSetting();
  if (!socials) {
    console.error('Unable to detect "SOCIAL" setting in .env');
    return;
  }
  await updateSocialSettings(location, profile);

  const folder = detectFolderSetting();
  if (!folder) {
    console.error('Unable to detect "FOLDER" setting in profile');
    console.error(profile);
    return;
  }
  await updateFolderSettings(location, profile, folder);
  await updateDbSettings(location, profile);
  await updateTweetSettings(location, profile);
  await updateIntervalSettings(location, profile);
  removeUndefined(profile);

  if (opts.validate) {
    const valid = profileIsValid(profile);
    console.log(`Profile is valid? ${valid}`);
    if (!valid) return;
  }

  if (opts.sync) {
    await syncProfile(profile);
  } else {
    await saveProfile(location, opts.name, profile);
    console.log(`Updated profile at ${location}`);
  }
})();
