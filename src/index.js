import "dotenv/config";
import { createServer } from "http";

const port = process.env.REDIRECT_PORT || process.env.PORT;
const app = createServer((req, res) => {
  res.writeHead(302, { location: process.env.REDIRECT_URL });
  res.end();
});
app.listen(port, () => {
  console.log(
    `waiting to redirect requests on port ${port} to ${process.env.REDIRECT_URL}`
  );
});
export default app;