import "dotenv/config"
import { createParser } from "dashdash";
import { groups, args } from "./const/settings.js";
import { analyzeFrames } from "./bin/analyze_frames.js";
const options = [
  groups.pathOptions,
  args.folder,
  args.results,
  groups.emptyOptions,
  args.help,
];
(async function () {
  const parser = createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("analyzeFrames.js %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true });
    console.log(
      "usage: npm run analyze-frames -- [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }
  analyzeFrames(opts.folder, opts.results);
})();
