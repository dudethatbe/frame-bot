import "dotenv/config";
import { createParser } from "dashdash";
import { groups, args } from "./const/settings.js";
import { postTweet } from "./bin/tweet.js";

const options = [
  groups.pathOptions,
  args.workspace,
  groups.profileOptions,
  args.name,
  args.help,
  groups.emptyOptions,
  args.debug,
];

(async function () {
  const parser = createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("tweet.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true });
    console.log("usage: npm run tweet --  [OPTIONS]\n" + "options:\n" + help);
    return;
  }

  if (opts.name) {
    await postTweet(opts.workspace, opts.name, opts.debug);
  }
})();
