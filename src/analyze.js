import "dotenv/config.js";
import dashdash from "dashdash";
import { groups, args } from "./const/settings.js"
import { analyzeVideosBin } from "./bin/analyze_videos.js";
const cliOptions = [
  groups.pathOptions,
  args.folder,
  args.results,
  groups.emptyOptions,
  args.help,
];
async function analyzeVideosCLI() {
  const parser = dashdash.createParser({ options: cliOptions });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("analyze.js %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log(
      "usage: npm run analyze-videos -- [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }
  console.log(await analyzeVideosBin(opts.folder, opts.results));
}
analyzeVideosCLI();