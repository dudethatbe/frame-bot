import { parse } from "path";

export function getAltTxt(profile, fullFilePath, fileKey) {
  let altTxt = "";
  var parsedFile = parse(fullFilePath);
  var parsedKey = parse(fileKey);
  var fileName = parsedFile.name;
  var dir = parsedKey.dir;
  var splitDir = dir.split(/\//);
  let parent = splitDir[splitDir.length - 1];
  let frameNumber = fileName.replace(parent, "").replace(/[^\d]/g, "");

  // format parent after using it for the replacement
  parent = parent.replace(/_/g, " ").trim();

  if (frameNumber.length > 0) {
    frameNumber = parseInt(frameNumber);
    const frameInterval = profile.FRAME_INTERVAL;
    const hasInterval = !isNaN(frameInterval);

    if (hasInterval) {
      frameNumber = frameNumber * frameInterval;
    }

    altTxt = `Frame #${frameNumber} from ${parent}`;

    if (!hasInterval) {
      altTxt = "Approximate " + altTxt;
    }
  } else {
    altTxt = `Frame from ${parent}`;
  }
  return altTxt;
}
