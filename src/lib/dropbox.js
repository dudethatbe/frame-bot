import { Dropbox } from "dropbox";
import { dirname, basename, resolve } from "path";
import { promises as fs } from "fs";
var dbx;
export function init(accessToken) {
  dbx = new Dropbox({ accessToken });
}
function download(filePath) {
  return new Promise((resolve, reject) => {
    dbx
      .filesDownload({ path: filePath })
      .then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          console.error(`Expected response "200" but got ${response.status}`);
          console.error(response);
          throw new Error(JSON.stringify(response));
        }
      })
      .catch(reject);
  });
}
function getFoldersContinue(cursor, continuePath, results, finish) {
  dbx
    .filesListFolderContinue({ cursor: cursor })
    .then((response) => {
      const entries = response.result.entries;
      results = results.concat(entries);
      if (response.result.has_more) {
        getFoldersContinue(
          response.result.cursor,
          continuePath,
          results,
          finish
        );
      } else {
        finish(results);
      }
    })
    .catch(finish);
}
async function getDropboxEntries(dbxPath) {
  return new Promise((resolve, reject) => {
    return dbx
      .filesListFolder({ path: dbxPath })
      .then((response) => {
        if (response.result.has_more) {
          getFoldersContinue(
            response.result.cursor,
            dbxPath,
            response.result.entries,
            resolve
          );
        } else {
          resolve(response.result.entries);
        }
      })
      .catch(reject);
  });
}
async function searchFolder(dbxPath, keys = new Set(), response = {}) {
  if (!response.hasOwnProperty("size")) {
    response["size"] = 0;
  }
  if (!response.hasOwnProperty("entries")) {
    response["entries"] = {};
  }
  const entries = await getDropboxEntries(dbxPath);
  // show how many entries are in this path
  // console.log(`${entries.length} from ${dbxPath}`);
  for (const entry of entries) {
    const entryPath = `${dbxPath}/${entry.name}`;
    let key = dirname(entry.path_lower);
    if (!keys.has(key)) {
      // use Set shortcut methods to check if its been used before
      keys.add(key);
      // store entries in an array per key
      response.entries[key] = [];
    }
    if (entry[".tag"] === "folder") {
      await searchFolder(entryPath, keys, response);
    } else {
      response.entries[key].push(entryPath);
      response.size += parseInt(entry.size);
    }
  }

  return response;
}
function chooseRandomEntry(entries) {
  const max = entries.length;
  const rand = Math.floor(Math.random() * Math.floor(max));
  return entries[rand];
}
export async function chooseRandomFrame(dropboxFolder) {
  const entries = await getDropboxEntries(dropboxFolder);
  // console.log(`got ${entries.length} entries from ${dropboxFolder}`);
  const folders = entries.filter((f) => {
    return f[".tag"] === "folder";
  });
  if (folders.length === 0) {
    // base case: return random entry
    const randomEntry = chooseRandomEntry(entries);
    return randomEntry;
  } else {
    // recursive case: search again with this folder
    const randomEntry = chooseRandomEntry(entries);
    return this.chooseRandomFrame(`${dropboxFolder}/${randomEntry.name}`);
  }
}
export async function countUploads(dropboxFolder) {
  let results;
  try {
    results = await searchFolder(dropboxFolder);
  } catch (e) {
    if (e.status === 409) {
      return { folders: [], files: [], size: 0, message: e.error };
    } else {
      throw e;
    }
  }
  const size = results.size;
  const entryKeys = Object.keys(results.entries);
  const nonEmptyFolders = [];
  const folders = entryKeys.filter((f) => {
    const hasFiles = results.entries[f].length > 0;
    if (hasFiles) nonEmptyFolders.push(results.entries[f]);
    return hasFiles;
  });

  const files = nonEmptyFolders.flat();

  return { folders, files, size };
}
export async function downloadFile(fullPathToFile, saveTo) {
  let response;
  try {
    response = await download(fullPathToFile);
  } catch (e) {
    console.error(`Unable to download ${fullPathToFile}`);
    throw e;
  }
  const fileName = basename(fullPathToFile);
  const file = resolve(saveTo, fileName);
  await fs.writeFile(file, response.result.fileBinary, "binary");
  return { file, response };
}
