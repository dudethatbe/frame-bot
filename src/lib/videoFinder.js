const ProbeCmd =
  "ffprobe -v error -show_streams -print_format json -show_entries format=duration,size:stream=width,height,duration,tags ";
import { resolve as _resolve } from "path";
import { promisify } from "util";
import { exec } from "child_process";
const promisifiedExec = promisify(exec);
import { promises } from "fs";
import findit from "findit";
const { access, mkdir, rmdir, readdir, lstat } = promises;
const { pow } = Math;

export function fileIsVideo(fileName) {
  return /webm|mpeg|mkv|flv|ogv|avi|mp4|m4a$/.test(fileName);
}
export function fileIsImage(fileName) {
  return /png|jpg|gif/.test(fileName);
}
export function isFolder(item) {
  return !fileIsImage(item) && !fileIsVideo(item);
}
function checkEntries(entries, entryTest) {
  return entries.some((entry) => {
    return entryTest(entry.name);
  });
}
export async function containsImages(folder) {
  const entries = await readdir(folder, { withFileTypes: true });
  return checkEntries(entries, fileIsImage);
}
export async function containsVideos(folder) {
  const entries = await readdir(folder, { withFileTypes: true });
  return checkEntries(entries, fileIsVideo);
}
export async function findSubtitleFile(folder, file, extension) {
  const re = new RegExp(`${file.replace(extension, "")}.*(srt|vtt)$`);
  const entries = await findEntries(folder, { files: true });

  const matches = entries.filter((e) => {
    return re.test(e);
  });

  if (matches.length < 1) {
    console.warn("Unable to find matching subtitle file with pattern below");
    console.warn(re);
    return "";
  } else {
    return matches[0];
  }
}
export async function getVideoFilesFromPath(pathToFiles) {
  let dirents;
  try {
    dirents = await readdir(pathToFiles, { withFileTypes: true });
  } catch (e) {
    if (e.code === "ENOENT") {
      console.error(`requested path "${pathToFiles}" does not exist`);
      return [];
    } else {
      throw e;
    }
  }
  const files = await Promise.all(
    dirents.map((dirent) => {
      const res = _resolve(pathToFiles, dirent.name);
      return dirent.isDirectory() ? getVideoFilesFromPath(res) : res;
    })
  );
  const response = Array.prototype.concat(...files).filter(fileIsVideo);
  return response;
}
export async function getImagesFromPath(pathToImages) {
  return findEntries(pathToImages, { files: true });
}
export async function getImagesFromPathAndIgnore(pathToImages, ignore) {
  return findEntries(pathToImages, { files: true, ignore: ignore });
}
function addToEntries(entry, entries, ignoring, ignoreStr) {
  if (ignoring) {
    if (entry.indexOf(ignoreStr) === -1) entries.push(entry);
  } else {
    entries.push(entry);
  }
}
const findEntries = async (parentFolder, options) => {
  const collectFiles = options.files === true;
  const collectFolders = options.folders === true;
  const ignoring = options.ignore != undefined;
  let ignore;
  if (ignoring) ignore = options.ignore.trim();
  const entries = [];
  const finder = findit(parentFolder);
  return new Promise(async (resolve, reject) => {
    if (collectFolders)
      finder.on("directory", async (dir) => {
        let result =
          (await containsImages(dir)) || (await containsVideos(dir));
        if (result) addToEntries(dir, entries, ignoring, ignore);
      });

    if (collectFiles)
      finder.on("file", (file) => {
        if (fileIsImage(file) || fileIsVideo(file))
          addToEntries(file, entries, ignoring, ignore);
      });

    finder.on("error", reject);
    finder.on("end", () => {
      resolve(entries);
    });
  });
};
export async function getFoldersFromPath(pathToFolders) {
  return findEntries(pathToFolders, { folders: true });
}
export async function getFoldersFromPathAndIgnore(pathToFolders, pathToIgnore) {
  return findEntries(pathToFolders, {
    folders: true,
    ignore: pathToIgnore,
  });
}
export async function getFilesFromPath(pathToFiles) {
  return findEntries(pathToFiles, { files: true });
}
export async function entryExists(entryPath) {
  try {
    await access(entryPath);
    return true;
  } catch (e) {
    if (e.code !== "ENOENT") console.error(e);

    return false;
  }
}
export async function createFolder(pathToFolder) {
  try {
    await mkdir(pathToFolder, { recursive: true });
    return true;
  } catch (e) {
    if (entryExists(pathToFolder)) {
      return true;
    } else {
      console.error(e);
      return false;
    }
  }
}
export async function deleteFolder(pathToFolder) {
  try {
    await rmdir(pathToFolder);
    return true;
  } catch (e) {
    if (!entryExists(pathToFolder)) {
      return true;
    } else {
      console.error(e);
      return false;
    }
  }
}
export async function probeVideo(pathToVideo) {
  const { stderr, stdout } = await promisifiedExec(`${ProbeCmd} -i "${pathToVideo}"`);
  if (stderr) {
    let ignorableErrors = [
      "co located POCs unavailable",
      "Missing reference picture, default is 0",
    ];
    let ignoringError = false;
    for (const errorMessageSnippet of ignorableErrors) {
      if (stderr.indexOf(errorMessageSnippet) > -1) {
        ignoringError = true;
      }
    }
    if (!ignoringError) {
      throw new Error(stderr);
    }
  }
  if (stdout) {
    return JSON.parse(stdout);
  } else {
    throw new Error(`empty stdout for ${pathToVideo}`);
  }
}
function hasDuration(stream) {
  if (stream.hasOwnProperty("duration")) {
    stream.duration = parseInt(stream.duration);
    return true;
  } else if (stream.hasOwnProperty("tags")) {
    const durationTags = Object.keys(stream.tags).filter(
      (t) => t === "duration" || t === "DURATION"
    );

    if (durationTags.length > -1) {
      // grab the first "duration" that appears in the tag list
      const tagName = durationTags[0];
      let originalDuration = stream.tags[tagName];
      // remove milliseconds if included
      const msIndex = originalDuration.indexOf(".");
      if (msIndex > -1) {
        originalDuration = originalDuration.slice(0, msIndex);
      }
      if (originalDuration.indexOf(":") === -1) {
        console.error(`unrecognized duration format "${originalDuration}"`);
        return false;
      } else {
        // split duration by ':' to collect HH:MM:SS
        const splitDuration = originalDuration
          .split(":")
          .map((s) => parseInt(s));
        const durationSeconds =
          splitDuration[2] +
          splitDuration[1] * 60 +
          splitDuration[0] * pow(60, 2);
        stream.duration = durationSeconds;
        return true;
      }
    } else {
      return false;
    }
  } else {
    return false;
  }
}
export function getVideoStream(details) {
  let videoStream = {};
  let dimensions = details.streams.filter((stream) => {
    return stream.hasOwnProperty("width") & stream.hasOwnProperty("height");
  });
  if (dimensions.length < 1) {
    console.error(details);
    throw new Error("Unable to detect height or width from details!");
  }
  videoStream["height"] = dimensions[0].height;
  videoStream["width"] = dimensions[0].width;

  if (hasDuration(dimensions[0])) {
    videoStream["duration"] = dimensions[0].duration;
  } else {
    // .mkv files like to store duration outside of the streams
    let format = details.format;
    if (hasDuration(format)) {
      videoStream["duration"] = format.duration;
    } else {
      console.error(details);
      throw new Error("Unable to detect duration in streams or in format");
    }
  }

  videoStream["size"] = details.format.size;
  return videoStream;
}
