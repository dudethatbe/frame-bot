import { MongoClient } from "mongodb";

export async function connectClient() {
  const url = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}?retryWrites=true&w=majority`;
  const client = new MongoClient(url);
  await client.connect();
  return client;
}
async function fetchLatestSortedPosts(posts, memory, field) {
  var sortField = {};
  sortField[field] = -1;
  return posts
    .aggregate([
      {
        $lookup: {
          from: "frames",
          localField: "frame_id",
          foreignField: "_id",
          as: "frame",
        },
      },
      {
        $sort: sortField,
      },
    ])
    .limit(memory)
    .toArray();
}
export async function getLatestPosts(posts, keys, memory) {
  var response = {};
  for (const key of keys) {
    var thesePosts = posts[key];
    var data = await fetchLatestSortedPosts(thesePosts, memory, "timestamp");
    if (data.length === 0) {
      // try old field
      data = await fetchLatestSortedPosts(thesePosts, memory, "tweetedAt");
    }
    response[key] = data;
  }
  return response;
}
export async function getRandomDocument(frames) {
  let choice = await frames
    .find({
      rnd: {
        $gte: Math.random(),
      },
    })
    .sort({ rnd: 1 })
    .limit(1)
    .toArray();

  if (choice.length < 1) {
    choice = await frames
      .find({ rnd: { $lte: Math.random() } })
      .sort({ rnd: 1 })
      .limit(1)
      .toArray();
  }
  if (choice.length < 1) {
    throw new Error("giving up!!!");
  }
  return choice[0];
}
