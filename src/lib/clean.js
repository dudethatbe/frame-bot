import { decode } from "./inkjet.js";
import { colord, extend } from "colord";
import a11yPlugin from "colord/plugins/a11y";
import { readFile, rename, stat } from "fs/promises";
import { resolve, parse } from "path";
import { floatToPercentStr, floatToNum } from "../const/util.js";

extend([a11yPlugin]);

function intToHex(int) {
  return Number(int).toString(16).padStart(2, "0");
}
function getHexColorCode(r, g, b, a) {
  return `#${intToHex(r)}${intToHex(g)}${intToHex(b)}${intToHex(a)}`;
}
function getFrameLuminance(chunk, results) {
  for (let i = 0; i < chunk.length; i += 4) {
    const rgbA = chunk.slice(i, i + 4);
    const cssName = getHexColorCode(...rgbA);
    const lumens = colord(cssName).luminance();
    if (results.hasOwnProperty(lumens)) {
      results[lumens]++;
    } else {
      results[lumens] = 1;
    }
  }
}
async function getLuminanceMap(imageBuffer, debug = false) {
  const minPercentage = 0.01;
  let luminanceMap = {};
  return new Promise((resolve, reject) => {
    decode(imageBuffer, (err, decoded) => {
      if (err) {
        if (debug) console.error(`caught error while decoding`);
        reject(err);
      } else {
        let data = decoded.data;
        getFrameLuminance(data, luminanceMap);
        let results = {};
        const keys = Object.keys(luminanceMap);
        const values = Object.values(luminanceMap);
        const total = values.reduce((prev, curr) => {
          return prev + curr;
        });

        for (const key of keys) {
          const count = luminanceMap[key];
          const share = count / total;
          if (share >= minPercentage) {
            results[key] = share;
          }
        }

        const sortable = [];
        for (var result in results) {
          sortable.push([result, results[result]]);
        }
        sortable.sort((a, b) => {
          return b[1] - a[1];
        });

        var sortedResults = {};
        sortable.forEach((sorted) => {
          if (sorted[0] === "0") {
            sortedResults["0.0"] = sorted[1];
          } else {
            sortedResults[sorted[0]] = sorted[1];
          }
        });
        resolve(sortedResults);
      }
    });
  });
}
function keysToString(keys) {
  let result = "[ ";
  const maxKeys = 3;
  let i = 0;
  let withinRange = i < keys.length && i < maxKeys;
  while (withinRange) {
    result += `${keys[i]}`;
    i++;
    withinRange = i < keys.length && i < maxKeys;
    if (i === keys.length || i === maxKeys) {
    } else {
      result += ", ";
    }
  }
  if (i < keys.length - 1) result += ", ...";
  return (result += " ]");
}
function luminanceMapToString(luminance) {
  let result = "{\n";
  const keys = Object.keys(luminance);
  const maxKeys = 3;
  let i = 0;
  while (i < keys.length && i < maxKeys) {
    let key = keys[i];
    let val = luminance[key];
    result += `\t${Number(key).toFixed(2)}=${floatToPercentStr(val)}`;
    if (i === keys.length || i === maxKeys) {
    } else {
      result += ", ";
    }
    i++;
  }
  if (i < keys.length - 1) result += "\t...";
  return (result += "\n}");
}
function imageShouldBeDeleted(luminance, percentage, tolerance, debug) {
  if (debug) console.log(luminanceMapToString(luminance));
  const keys = Object.keys(luminance);

  if (keys.length === 1) {
    const key = keys[0];
    const meetsTolerance = key <= tolerance;
    if (debug)
      console.log(`key ${key} <= tolerance ${tolerance} ? (${meetsTolerance})`);
    return meetsTolerance;
  }

  const intolerableKeys = keys.filter((k) => k > tolerance);
  if (intolerableKeys.length > 0) {
    if (debug)
      console.log(
        `found ${intolerableKeys.length
        } key(s) > threshold ${tolerance}\t${keysToString(intolerableKeys)}`
      );
    return false;
  }

  const dominantPercentage = floatToNum(luminance[keys[0]]);
  if (dominantPercentage < percentage) {
    if (debug)
      console.log(
        `dominant percentage ${dominantPercentage} < percentage ${percentage}`
      );
    return false;
  }

  return true;
}

async function mapImagesToStats(images) {
  let mapped = [];
  for (let image of images) {
    let imageStat = await stat(image);
    mapped.push({ size: imageStat.size, name: image });
  }
  return mapped;
}
async function cleanSortedFrames(
  sortedFrames,
  trash,
  tries,
  percentage,
  tolerance,
  debug
) {
  let lightImagesDetected = 0;
  for (let image of sortedFrames) {
    var buf;
    try {
      buf = await readFile(image.name);
    } catch (e) {
      if (e.code != "ENOENT") {
        console.error("caught error while trying to read a file");
        throw e;
      }
      // assuming the file is missing, and has already been processed
      continue;
    }
    if (debug) console.time(image.name);

    const luminance = await getLuminanceMap(buf, debug);
    if (debug) console.timeEnd(image.name);

    if (imageShouldBeDeleted(luminance, percentage, tolerance, debug)) {
      if (debug) console.log("deleting", image.name);
      const movedFile = resolve(trash, parse(image.name).base);
      try {
        await rename(image.name, movedFile);
      } catch (e) {
        if (e.code != "ENOENT") {
          console.error("caught error while moving file to trash");
          throw e;
        }
      }
      // reset image detection
      lightImagesDetected = 0;
    } else {
      lightImagesDetected++;
    }

    if (lightImagesDetected >= tries) {
      break;
    }
  }
}
async function sortAndClean(
  stats,
  sortFunc,
  trash,
  tries,
  percentage,
  tolerance,
  debug
) {
  var sorted = stats.sort(sortFunc);

  await cleanSortedFrames(sorted, trash, tries, percentage, tolerance, debug);
}
function sortBySize(a, b) {
  return a.size - b.size;
}
function sortByNameAsc(a, b) {
  return a.name.normalize().localeCompare(b.name.normalize());
}
function sortByNameDesc(a, b) {
  return b.name.normalize().localeCompare(a.name.normalize());
}
export async function cleanFrames(
  imageFiles,
  trash,
  percentage,
  tolerance,
  tries,
  quick,
  debug
) {
  const imageStats = await mapImagesToStats(imageFiles);
  if (quick) {
    if (debug) console.log("sorting by size");
    await sortAndClean(
      imageStats,
      sortBySize,
      trash,
      tries,
      percentage,
      tolerance,
      debug
    );
  } else {
    if (debug) console.log("- sorting by name asc");

    await sortAndClean(
      imageStats,
      sortByNameAsc,
      trash,
      tries,
      percentage,
      tolerance,
      debug
    );
    if (debug) console.log("- sorting by name desc");
    await sortAndClean(
      imageStats,
      sortByNameDesc,
      trash,
      tries,
      percentage,
      tolerance,
      debug
    );
    if (debug) console.log("- sorting by size");
    await sortAndClean(
      imageStats,
      sortBySize,
      trash,
      tries,
      percentage,
      tolerance,
      debug
    );
  }
}
const _getFrameLuminance = async function (framePath) {
  const buf = await readFile(framePath);
  return getLuminanceMap(buf);
};
export { _getFrameLuminance as getFrameLuminance };
