import { getAltTxt } from './alt_txt.js';

import { Jimp } from "jimp";

import Proto from '@atproto/api'
const { BskyAgent } = Proto

import { readFile } from "fs/promises";

export async function postBsky(profile, file, key) {
  const altTxt = getAltTxt(profile, file, key);
  const agent = new BskyAgent({ service: profile.BLUESKY_SERVICE });

  await agent.login({
    identifier: profile.BLUESKY_IDENTIFIER,
    password: profile.BLUESKY_PASSWORD
  });

  const imgData = await readFile(file);
  const image =  await Jimp.read(imgData);
  const width = image.width;
  const height = image.height;

  const blob = await agent.api.com.atproto.repo.uploadBlob(imgData, { encoding: 'image/jpeg' })

  const post = await agent.post({
    text: "",
    embed: {
      images: [{
        image: blob.data.blob,
        alt: altTxt,
        aspectRatio:{
          width,
          height
        } 
      }],
      $type: 'app.bsky.embed.images'
    }
  });

  return post;
}