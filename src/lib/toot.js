import { login } from "masto";
import { getAltTxt } from "./alt_txt.js";
import { createReadStream } from "fs";
async function newClient(url, accessToken) {
  const masto = await login({
    url,
    accessToken,
    disableVersionCheck: true,
    timeout: 6000,
  });
  return masto;
}
export async function postToot(profile, file, key) {
  const altTxt = getAltTxt(profile, file, key);
  process.env.CLIENT_KEY = profile.MASTODON_CLIENT_KEY ? profile.MASTODON_CLIENT_KEY : profile.CLIENT_KEY;
  process.env.CLIENT_SECRET = profile.MASTODON_CLIENT_SECRET ? profile.MASTODON_CLIENT_SECRET : profile.CLIENT_SECRET;
  const client = await newClient(
    profile.MASTODON_URL,
    profile.MASTODON_ACCESS_TOKEN ? profile.MASTODON_ACCESS_TOKEN : profile.ACCESS_TOKEN
  );
  const attachment = await client.mediaAttachments.create({
    file: createReadStream(file),
    description: altTxt,
  });
  const status = await client.statuses.create({
    sensitive: false,
    status: altTxt,
    mediaIds: [attachment.id],
    visibility: "public",
  });
  return status;
}
