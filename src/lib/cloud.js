import { fetchFrameFromDb } from "../bin/tweet_mongo.js";
import { init as initS3, downloadObject } from "./s3.js";
import { init as initDBX, downloadFile } from "./dropbox.js";
export async function chooseFileFromDb(location, profile, social, debug) {
  const source = profile["SOURCE"];
  let download;
  switch (source) {
    case "S3":
      download = downloadObject;
      initS3(
        profile.AWS_ACCESS_KEY_ID,
        profile.AWS_SECRET_ACCESS_KEY,
        profile.ENDPOINT,
        profile.REGION
      );
      break;
    case "DBX":
      download = downloadFile;
      initDBX(profile.DBX_ACCESS_TOKEN);
      break;
    default:
      throw new Error(`Unrecognized SOURCE ${source}`);
  }
  const frame = await fetchFrameFromDb(profile, social, debug);
  const key = frame.name;
  let file;
  switch (source) {
    case "S3":
      file = await download(profile.BUCKET, key, location);
      console.log(`Downloaded ${key}`);
      break;
    case "DBX":
      const data = await download(key, location);
      // TODO: why not use .file above?
      file = data.file;
      console.log(`Downloaded ${file}`);
      break;
    default:
      throw new Error(`Unrecognized SOURCE ${source}`);
  }

  return { frame, file, key };
}
