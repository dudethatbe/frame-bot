import { promises } from "fs";
const { readFile } = promises;
import { parse } from "path";
import { TwitterApi } from "twitter-api-v2";
import { getAltTxt } from "./alt_txt.js";

export async function postTweet(profile, fullFilePath, fileKey) {
  const altTxt = getAltTxt(profile, fullFilePath, fileKey);
  const fileType = parse(fullFilePath).ext;
  const data = await readFile(fullFilePath);

  const client = new TwitterApi({
    appKey: profile.TWITTER_CONSUMER_KEY ? profile.TWITTER_CONSUMER_KEY : profile.CONSUMER_KEY,
    appSecret: profile.TWITTER_CONSUMER_SECRET ? profile.TWITTER_CONSUMER_SECRET : profile.CONSUMER_SECRET,
    accessToken: profile.TWITTER_ACCESS_TOKEN_KEY ? profile.TWITTER_ACCESS_TOKEN_KEY : profile.ACCESS_TOKEN_KEY,
    accessSecret: profile.TWITTER_ACCESS_TOKEN_SECRET ? profile.TWITTER_ACCESS_TOKEN_SECRET : profile.ACCESS_TOKEN_SECRET
  });

  // upload media
  let mediaId;
  try {
    mediaId = await client.v1.uploadMedia(Buffer.from(data), { mimeType: fileType });
  } catch (e) {
    console.error(e);
    throw e;
  }

  // add metadata
  try {
    await client.v1.createMediaMetadata(mediaId, { alt_text: { text: altTxt } });
  } catch (e) {
    console.error(e);
    throw e;
  }

  // post tweet with media and metadata
  let response;
  try {
    response = await client.v2.tweet({ text: "", media: { media_ids: [mediaId] } });
  } catch (e) {
    console.error(e);
    throw e;
  }
  return response;
}
