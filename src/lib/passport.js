import passport from "passport";
export const { use, serializeUser, deserializeUser, initialize, authenticate } = passport;