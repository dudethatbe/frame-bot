// import { writeFile } from "fs";
import { readFile, unlink, writeFile } from "fs/promises";
import { resolve } from "path";
import { findProfile } from "../bin/populate_mongo.js";
const TWITTER_ACCESS_TOKEN_KEY = "TWITTER_ACCESS_TOKEN_KEY";
const TWITTER_ACCESS_TOKEN_SECRET = "TWITTER_ACCESS_TOKEN_SECRET";
const MASTODON_ACCESS_TOKEN = "MASTODON_ACCESS_TOKEN";
const MASTODON_URL = "MASTODON_URL";
const MASTODON_CLIENT_KEY = "MASTODON_CLIENT_KEY";
const MASTODON_CLIENT_SECRET = "MASTODON_CLIENT_SECRET";
const TWITTER_CONSUMER_API_KEY = "TWITTER_CONSUMER_KEY";
const TWITTER_CONSUMER_API_SECRET = "TWITTER_CONSUMER_SECRET";
const BLUESKY_IDENTIFIER = "BLUESKY_IDENTIFIER";
const BLUESKY_PASSWORD = "BLUESKY_PASSWORD";
const BLUESKY_SERVICE = "BLUESKY_SERVICE";
const ACCESS_TOKEN = "DBX_ACCESS_TOKEN";
const AWS_SECRET_ACCESS_KEY = "AWS_SECRET_ACCESS_KEY";
const AWS_ACCESS_KEY_ID = "AWS_ACCESS_KEY_ID";
const ENDPOINT = "ENDPOINT";
const FOLDER = "FOLDER";
const REGION = "REGION";
const BUCKET = "BUCKET";
const SOURCE = "SOURCE";
const SOCIAL = "SOCIAL";
const TWITTER = "TWITTER";
const MASTODON = "MASTODON";
const BLUESKY = "BLUESKY";
const USERNAME = "USERNAME";
const DB_NAME = "DB_NAME";
const TWEET_THRESHOLD = "TWEET_THRESHOLD";
const TWEET_MEMORY = "TWEET_MEMORY";
const TWEET_DEPTH = "TWEET_DEPTH";
const TWEET_TRIM_NAME = "TWEET_TRIM_NAME";
const INTERVAL = "FRAME_INTERVAL";
const TWITTER_KEYS = [
  TWITTER_ACCESS_TOKEN_KEY,
  TWITTER_ACCESS_TOKEN_SECRET,
  TWITTER_CONSUMER_API_KEY,
  TWITTER_CONSUMER_API_SECRET
];
const MASTODON_KEYS = [
  MASTODON_ACCESS_TOKEN,
  MASTODON_URL,
  MASTODON_CLIENT_KEY,
  MASTODON_CLIENT_SECRET,
];
const BLUESKY_KEYS = [
  BLUESKY_IDENTIFIER,
  BLUESKY_PASSWORD,
  BLUESKY_SERVICE
];
const DB_KEYS = [DB_NAME];
const DBX_KEYS = [ACCESS_TOKEN];
const S3_KEYS = [
  AWS_SECRET_ACCESS_KEY,
  AWS_ACCESS_KEY_ID,
  ENDPOINT,
  REGION,
  BUCKET,
];
const TWEET_KEYS = [
  TWEET_THRESHOLD,
  TWEET_MEMORY,
  TWEET_DEPTH,
  TWEET_TRIM_NAME,
];
const OLD_TWITTER_KEYS = [
  "CONSUMER_KEY",
  "CONSUMER_SECRET",
  "ACCESS_TOKEN_KEY",
  "ACCESS_TOKEN_SECRET"
];
const OLD_MASTODON_KEYS = [
  "CLIENT_KEY",
  "CLIENT_SECRET",
  "ACCESS_TOKEN",
  "MASTODON_URL"
];

const FRAME_KEYS = [INTERVAL];
function setToProfile(a, b, keys) {
  for (const key of keys) {
    if (!isNullish(b[key]))
      a[key] = b[key];
  }
}

function getSource() {
  return process.env[SOURCE];
}

function getSocial() {
  return process.env[SOCIAL];
}

function getFolder() {
  return process.env[FOLDER];
}

function getS3Settings() {
  var profile = {};
  setToProfile(profile, process.env, S3_KEYS);
  profile[SOURCE] = "S3";
  return profile;
}
function applyS3Settings(profile, settings) {
  setToProfile(profile, settings, S3_KEYS);
  profile[SOURCE] = "S3";
}

function getTweetSettings() {
  var profile = {};
  setToProfile(profile, process.env, TWEET_KEYS);
  return profile;
}
function applyTweetSettings(profile, settings) {
  setToProfile(profile, settings, TWEET_KEYS);
}
function getFrameSettings() {
  var profile = {};
  setToProfile(profile, process.env, FRAME_KEYS);
  return profile;
}
function applyFrameSettings(profile, settings) {
  setToProfile(profile, settings, FRAME_KEYS);
}

function getDropboxSettings() {
  var profile = {};
  setToProfile(profile, process.env, DBX_KEYS);
  profile[SOURCE] = "DBX";
  return profile;
}

function applyDropboxSettings(profile, settings) {
  setToProfile(profile, settings, DBX_KEYS);
  profile[SOURCE] = "DBX";
}

function getTwitterSettings() {
  var profile = {};
  setToProfile(profile, process.env, TWITTER_KEYS);
  if (!validateProfileSettings(profile, TWITTER_KEYS)) {
    setToProfile(profile, process.env, OLD_TWITTER_KEYS)
  }
  return profile;
}

function applyTwitterSettings(profile, settings) {
  setToProfile(profile, settings, TWITTER_KEYS);
  if (!validateProfileSettings(profile, TWITTER_KEYS)) {
    setToProfile(profile, settings, OLD_TWITTER_KEYS);
  }
}

function getMastodonSettings() {
  var profile = {};
  setToProfile(profile, process.env, MASTODON_KEYS);
  if (!validateProfileSettings(profile, MASTODON_KEYS)) {
    setToProfile(profile, process.env, OLD_MASTODON_KEYS);
  }
  return profile;
}

function applyMastodonSettings(profile, settings) {
  setToProfile(profile, settings, MASTODON_KEYS);
  if (!validateProfileSettings(profile, MASTODON_KEYS)) {
    setToProfile(profile, settings, OLD_MASTODON_KEYS);
  }
}

function getBlueSkySettings() {
  var profile = {};
  setToProfile(profile, process.env, BLUESKY_KEYS);
  return profile;
}

function applyBlueSkySettings(profile, settings) {
  setToProfile(profile, settings, BLUESKY_KEYS);
}

function getDbSettings() {
  var profile = {};
  // don't store every db connection detail, just the name of the database profile uses
  setToProfile(profile, process.env, DB_KEYS);
  return profile;
}

function applyDbSettings(profile, settings) {
  setToProfile(profile, settings, DB_KEYS);
}

function getProfile(location, username) {
  return resolve(location, username + "-profile.json");
}

function isNullish(value) {
  const float = parseFloat(value);
  if (!isNaN(float)) {
    return float < 0; // value must be a number
  } else {
    return value === "" || value === null || value === undefined; // value must be string
  }
}

function validateProfileSettings(profile, keys) {
  const profileKeys = Object.keys(profile);

  const missingKeys = keys.filter((k) => profileKeys.indexOf(k) === -1);
  if (missingKeys.length > 0) {
    return false;
  }

  for (const key of keys) {
    const value = profile[key];
    if (isNullish(value)) {
      console.error(`Invalid ${key} value stored in profile`);
      return false;
    }
  }

  return true;
}

async function writeToProfile(profilePath, data) {
  try {
    await unlink(profilePath);
  } catch (e) {
    if (e.code !== "ENOENT") {
      throw e;
    }
  }
  return writeFile(profilePath, JSON.stringify(data, null, 2), () => { });
}

function dropboxSettingsValid(profile) {
  return validateProfileSettings(profile, DBX_KEYS);
}

function s3SettingsValid(profile) {
  return validateProfileSettings(profile, S3_KEYS);
}

function tweetSettingsValid(profile) {
  return validateProfileSettings(profile, TWEET_KEYS);
}

function mastodonSettingsValid(profile) {
  if (!validateProfileSettings(profile, MASTODON_KEYS)) {
    console.log('trying old mastodon keys')
    return validateProfileSettings(profile, OLD_MASTODON_KEYS);
  }
  return true;
}

function twitterSettingsValid(profile) {
  if (!validateProfileSettings(profile, TWITTER_KEYS)) {
    console.log('trying old twitter keys')
    return validateProfileSettings(profile, OLD_TWITTER_KEYS);
  }
  return true;
}

function isMissingOrEmpty(key, keys, profile) {
  if (keys.indexOf(key) === -1) {
    console.error(`Profile is missing ${key}`);
    return true;
  }
  if (profile[key] === "") {
    console.error(`Profile "${key}" is empty`);
    return true;
  }
  return false;
}

export function detectSourceSetting() {
  return getSource();
}

function applySocialSettings(profile, settings, keys) {
  setToProfile(profile, settings, keys);
}

function hasManySocial(socials) {
  return socials.split(",").length > 1;
}

export function detectSocialSetting() {
  const socials = getSocial();
  if (hasManySocial(socials)) {
    return socials.split(",");
  } else {
    return [socials];
  }
}

export function detectFolderSetting() {
  return getFolder();
}
export function profileIsValid(profile) {
  const keys = Object.keys(profile);

  if (isMissingOrEmpty(USERNAME, keys, profile)) return false;

  if (isMissingOrEmpty(FOLDER, keys, profile)) return false;

  if (isMissingOrEmpty(SOURCE, keys, profile)) return false;

  const source = profile[SOURCE];
  if (source === "S3") {
    if (!s3SettingsValid(profile)) {
      console.error("Unable to verify S3 Settings in profile");
      return false;
    }
  } else if (source == "DBX") {
    if (!dropboxSettingsValid(profile)) {
      console.error("Unable to verify Dropbox Settings in profile");
      return false;
    }
  } else {
    console.error("Source does not equal S3 or DBX");
    return false;
  }

  for (const social of profile.SOCIAL.split(",")) {
    if (social === "TWITTER") {
      if (!twitterSettingsValid(profile)) {
        console.error("Unable to verify twitter settings in profile");
        return false;
      }
    } else if (social === "MASTODON") {
      if (!mastodonSettingsValid(profile)) {
        console.error("Unable to verify mastodon settings in profile");
        return false;
      }
    }
  }
  if (isMissingOrEmpty(DB_NAME, keys, profile)) return false;

  if (!tweetSettingsValid(profile)) return false;

  return true;
}

export function makeProfile(options) {
  var profile = {};
  profile.USERNAME = options.folder;
  profile.FOLDER = options.folder;
  // profile.ACCESS_TOKEN_KEY = options.access_token_key;
  // profile.ACCESS_TOKEN_SECRET = options.access_token_secret;
  const source = options.source;
  profile.SOURCE = source;
  switch (source) {
    case "S3":
      profile.BUCKET = options.bucket;
      profile.REGION = options.region;
      profile.ENDPOINT = options.endpoint;
      profile.AWS_SECRET_ACCESS_KEY = options.aws_access_key;
      profile.AWS_ACCESS_KEY_ID = options.aws_secret_key;
      break;
    case "DBX":
      profile.DBX_ACCESS_TOKEN = options.dbx_access_token;
      break;
  }

  profile.DB_NAME = options.db_name;
  if (validateProfileSettings(profile, source === "S3" ? S3_KEYS : DBX_KEYS)) {
    return profile;
  }
  console.error(profile);
  throw new Error("invalid profile");
}

export async function readProfile(location, username) {
  const profilePath = getProfile(location, username);
  // placeholder data to return JSON
  var data = `{"USERNAME": "${username}"}`;
  try {
    data = await readFile(profilePath);
    return JSON.parse(data);
  } catch (e) {
    if (e.code != "ENOENT") {
      throw e;
    }
    return JSON.parse(data);
  }
}

export async function fetchProfile(username) {
  const profile = await findProfile(username);
  return profile;
}

export async function tryFetchProfile(workspace, username) {
  var profile = await fetchProfile(username);
  if (!profile) {
    profile = await readProfile(workspace, username);
    if (Object.keys(profile).length < 2) {
      throw Error("Unable to find profile");
    }
  }
  return profile;
}
export async function updateProfileMastodon(
  location,
  username,
  mastodonData,
  clientKey,
  clientSecret
) {
  const profile = await readProfile(location, username);

  profile[USERNAME] = username;
  profile[MASTODON_ACCESS_TOKEN] = mastodonData.ACCESS_TOKEN;
  profile[MASTODON_CLIENT_KEY] = clientKey;
  profile[MASTODON_CLIENT_SECRET] = clientSecret;

  return writeFile(
    getProfile(location, username),
    JSON.stringify(profile, null, 2),
    () => { }
  );
}
export async function updateProfileTwitter(location, username, twitterData, oldVars) {
  const profile = await readProfile(location, username);

  profile[USERNAME] = username;
  if (oldVars) {
    profile["ACCESS_TOKEN_KEY"] = twitterData.ACCESS_TOKEN_KEY;
    profile["ACCESS_TOKEN_SECRET"] = twitterData.ACCESS_TOKEN_SECRET;
    profile["CONSUMER_KEY"] = twitterData.CONSUMER_KEY;
    profile["CONSUMER_SECRET"] = twitterData.CONSUMER_SECRET;
  } else {
    profile[TWITTER_ACCESS_TOKEN_KEY] = twitterData.ACCESS_TOKEN_KEY;
    profile[TWITTER_ACCESS_TOKEN_SECRET] = twitterData.ACCESS_TOKEN_SECRET;
    profile[TWITTER_CONSUMER_API_KEY] = twitterData.CONSUMER_KEY;
    profile[TWITTER_CONSUMER_API_SECRET] = twitterData.CONSUMER_SECRET;
  }

  return saveProfile(location, username, profile);
}

export async function saveProfile(location, username, profile) {
  return writeFile(
    getProfile(location, username),
    JSON.stringify(profile, null, 2),
    () => { }
  );
}

export async function formatProfile(location, username, profile) {
  for (const key of Object.keys(profile)) {
    if (key != "USERNAME") {
      delete profile[key]
    }
  }
  await saveProfile(location, username, profile);
}
export async function updateTweetSettings(location, profile) {
  const settings = getTweetSettings();
  applyTweetSettings(profile, settings);
  return writeToProfile(getProfile(location, profile[USERNAME]), profile);
}

export async function updateIntervalSettings(location, profile) {
  const settings = getFrameSettings();
  applyFrameSettings(profile, settings);
  return writeToProfile(getProfile(location, profile[USERNAME]), profile);
}

export async function updateS3Settings(location, profile) {
  const settings = getS3Settings();
  applyS3Settings(profile, settings);
  trimDbxSettings(profile);
  return writeToProfile(getProfile(location, profile[USERNAME]), profile);
}

export async function updateDbxSettings(location, profile) {
  const settings = getDropboxSettings();
  applyDropboxSettings(profile, settings);
  trimS3Settings(profile);
  return writeToProfile(getProfile(location, profile[USERNAME]), profile);
}

export async function updateSocialSettings(location, profile) {
  // take the socials defined in .env
  const socials = detectSocialSetting();
  // and for each value, take the related keys
  var socialSettings = { SOCIAL: process.env[SOCIAL] };
  var socialKeys = "";
  for (const social of socials) {
    switch (social) {
      case TWITTER:
        socialKeys += TWITTER_KEYS.concat(OLD_TWITTER_KEYS).join(",") + ",";
        applyTwitterSettings(socialSettings, getTwitterSettings());
        break;
      case MASTODON:
        socialKeys += MASTODON_KEYS.concat(OLD_MASTODON_KEYS).join(",") + ",";
        applyMastodonSettings(socialSettings, getMastodonSettings());
        break;
      case BLUESKY:
        socialKeys += BLUESKY_KEYS.join(",") + ",";
        applyBlueSkySettings(socialSettings, getBlueSkySettings());
        break;
      default:
        console.warn(`Unrecognized "social" value detected ${social}`);
    }
  }
  socialKeys += "SOCIAL";
  applySocialSettings(profile, socialSettings, socialKeys.split(","));
  return writeToProfile(getProfile(location, profile[USERNAME]), profile);
}

export async function updateTwitterSettings(location, profile) {
  const settings = getTwitterSettings();
  if (Object.values(settings).includes(undefined)) {
    console.error('Detected an "undefined" value in the TWEET settings');
    return;
  }
  applyTwitterSettings(profile, settings);
  return writeToProfile(getProfile(location, profile[USERNAME]), profile);
}

export async function updateMastodonSettings(location, profile) {
  const settings = getMastodonSettings();
  if (Object.values(settings).includes(undefined)) {
    console.error('Detect an "undefined" value in the TOOT settings');
    return;
  }
  applyMastodonSettings(profile, settings);
  return writeToProfile(getProfile(location, profile[USERNAME]), profile);
}

export async function updateDbSettings(location, profile) {
  const settings = getDbSettings();
  applyDbSettings(profile, settings);
  return writeToProfile(getProfile(location, profile[USERNAME]), profile);
}

export function trimS3Settings(profile) {
  for (const key of S3_KEYS) {
    delete profile[key];
  }
}

export function trimDbxSettings(profile) {
  for (const key of DBX_KEYS) {
    delete profile[key];
  }
}

export function removeUndefined(profile) {
  for (const key of Object.keys(profile)) {
    if (profile[key] === null || profile[key] === undefined) {
      delete profile[key];
    }
  }
}
export async function updateSource(location, profile, source) {
  const username = profile[USERNAME];
  profile[SOURCE] = source;
  return writeToProfile(getProfile(location, username), profile);
}

export async function updateFolderSettings(location, profile, folder) {
  const username = profile[USERNAME];
  profile[FOLDER] = folder;
  return writeToProfile(getProfile(location, username), profile);
}

export async function updateDbNameSettings(location, profile, dbName) {
  const username = profile[USERNAME];
  profile[DB_NAME] = dbName;
  return writeToProfile(getProfile(location, username), profile);
}
