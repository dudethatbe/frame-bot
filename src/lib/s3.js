import { createWriteStream } from "fs";
import { resolve as _resolve, basename } from "path";
import AWS from "aws-sdk";
var s3;
export function init(accessKeyId, secretAccessKey, endpoint, region) {
  process.env.AWS_ACCESS_KEY_ID = accessKeyId;
  process.env.AWS_SECRET_ACCESS_KEY = secretAccessKey;
  s3 = new AWS.S3({
    endpoint: new AWS.Endpoint(endpoint),
    region,
  });
}
async function listObjects(bucket, folder, token) {
  var params = {
    Bucket: bucket,
    Prefix: folder,
  };
  if (token) {
    params["ContinuationToken"] = token;
  }

  try {
    return s3.listObjectsV2(params).promise();
  } catch (e) {
    console.error(e);
  }
}
export async function getObjects(bucket, folder) {
  let objects = [];
  let response = await listObjects(bucket, folder);
  let contents = response.Contents;
  objects = objects.concat(contents);
  while (response.IsTruncated) {
    let token = response.NextContinuationToken;
    response = await listObjects(bucket, folder, token);
    objects = objects.concat(response.Contents);
  }
  return objects;
}
export async function chooseRandomObject(bucket, folder) {
  const objects = await this.getObjects(bucket, folder);
  const rand = Math.floor(Math.random() * objects.length);
  return objects[rand];
}
export async function downloadObject(bucket, key, dest) {
  const file = _resolve(dest, basename(key));
  const write = createWriteStream(file);
  return new Promise((resolve, reject) => {
    s3.getObject({ Bucket: bucket, Key: key })
      .createReadStream()
      .on("end", () => {
        resolve(_resolve(file));
      })
      .on("error", (e) => {
        reject(e);
      })
      .pipe(write);
  });
}
