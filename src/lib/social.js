import { insertPost } from "../bin/tweet_mongo.js";
import { postToot } from "./toot.js";
import { postTweet } from "./twitter.js";
import { postBsky } from "./bsky.js";
const TwitterFields = [
  "id",
  "text",
];
const MastodonFields = [
  "id",
  "createdAt",
  "url",
  "favouritesCount",
  "reblogsCount",
  "mediaAttachments",
];
const BlueSkyFields = [
  "cid",
  "uri"
]
function assignPostFields(fields, post, response) {
  for (const field of fields) {
    post[field] = response[field];
  }
}
const _insertPost = async (dbName, social, socialPost, frame) => {
  const post = {
    timestamp: new Date(),
    frame_id: frame._id,
    tries: frame.tries,
    post: socialPost,
  };
  await insertPost(dbName, social, post);
};
export { _insertPost as insertPost };
export async function postSocialMedia(profile, social, file, key) {
  var post = {};
  var fullPost = {};
  switch (social) {
    case "TWITTER":
      fullPost = await postTweet(profile, file, key);
      assignPostFields(TwitterFields, post, fullPost.data);
      console.log(post.text);
      break;
    case "MASTODON":
      fullPost = await postToot(profile, file, key);
      assignPostFields(MastodonFields, post, fullPost);
      console.log(post.url);
      break;
    case "BLUESKY":
      fullPost = await postBsky(profile, file, key);
      assignPostFields(BlueSkyFields, post, fullPost);
      console.log(post)
      break;
    default:
      break;
  }
  return post;
}
