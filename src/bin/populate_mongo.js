import { connectClient } from "../lib/mongo.js";
const USERS = "users";
const PROFILES = "profiles";
export async function findProfile(username) {
  const client = await connectClient();
  const appDb = client.db(USERS);
  const collection = appDb.collection(PROFILES);
  const profileRecord = await collection.findOne({
    USERNAME: { $eq: username },
  });
  await client.close();
  return profileRecord;
}
export async function syncProfile(profile) {
  const client = await connectClient();
  const appDb = client.db(USERS);
  const collection = appDb.collection("profiles");
  const profileRecord = await collection.findOne({
    USERNAME: { $eq: profile.USERNAME },
  });
  if (profileRecord) {
    const id = profileRecord["_id"];
    const findFilter = { _id: id };
    const profileKeys = Object.keys(profile);
    // "_id" isn't stored locally, so pretend that it's a part of the profile
    profileKeys.push("_id");
    const recordKeys = Object.keys(profileRecord);
    // find keys stored in db that should be removed (switching sources)
    const unsetKeys = recordKeys.filter((r) => profileKeys.indexOf(r) === -1);
    if (unsetKeys.length > 0) {
      console.log(
        `found ${unsetKeys.lenght} keys stored in db that are not in current profile ...`
      );
      console.log(`unsetting remote field ${unsetKeys.join(", ")}`);
      const updateObject = {};
      for (const key of unsetKeys) {
        updateObject[key] = 1;
      }
      await collection.updateOne(findFilter, { $unset: updateObject });
    }
    await collection.updateOne(findFilter, { $set: profile });
    console.log(`updated profile ${profile.USERNAME}`);
  } else {
    await collection.insertOne(profile);
    console.log(`created profile ${profile.USERNAME}`);
  }
  await client.close();
}
export async function updateMongoDB(dbName, frames) {
  const client = await connectClient();
  const appDb = client.db(dbName);
  const collectionName = "frames";
  const collection = appDb.collection(collectionName);

  try {
    await collection.drop();
  } catch (e) {
    if (e.codeName !== "NamespaceNotFound") {
      console.log(e.ok, e.code, e.codeName);
      await client.close();
      throw e;
    }
  }

  console.time(dbName);
  console.log(`inserting ${frames.length} frames into db ${dbName} ...`);
  try {
    await collection.insertMany(frames);
  } catch (e) {
    console.log("caught error while inserting frames");
    console.log(e.ok, e.code, e.codeName);
    await client.close();
    throw e;
  }
  console.timeEnd(dbName);

  await collection.createIndex({ rnd: 1 });
  await collection.createIndex({ name: 1 });

  await client.close();
  return;
}
