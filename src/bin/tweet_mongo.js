import { connectClient, getRandomDocument, getLatestPosts } from "../lib/mongo.js";
function trimName(name, trim) {
  // if trim is longer than the name, than return the last character
  if (trim >= name.length) {
    return name.slice(trim - 1);
  }
  return name.slice(trim);
}

function splitFrameName(name, depth) {
  let split = name.split("/");
  // if depth is deeper than the length, return the last one
  if (depth >= split.length) {
    return split[split.length - 1];
  }
  return split[depth];
}
function getLatestFrameNames(latestFrames, depth, trim) {
  return latestFrames.map((frame) => {
    return trimName(splitFrameName(frame.name, depth), trim);
  });
}
function getLatestFrames(latestPosts) {
  var response = [];
  for (const key of Object.keys(latestPosts)) {
    var data = latestPosts[key].map((x) => {
      const keys = Object.keys(x);
      if (keys.indexOf("timestamp") > -1) {
        return { name: x.frame[0].name, data: x.timestamp };
      } else {
        return { name: x.frame[0].name, date: x.tweetedAt };
      }
    });
    response = response.concat(data);
  }
  return response;
}
async function getRandomFrameFromProfile(profile, frames, posts, debug) {
  return getRandomFrame(
    frames,
    posts,
    Number(profile.TWEET_THRESHOLD),
    Number(profile.TWEET_MEMORY),
    Number(profile.TWEET_DEPTH),
    Number(profile.TWEET_TRIM_NAME),
    debug
  );
}
async function getRandomFrame(
  frames,
  posts,
  threshold,
  memory,
  depth,
  trim,
  debug
) {
  const frameCount = await frames.count({});
  var postsCount = 0;
  const postsKeys = Object.keys(posts);
  for (const key of postsKeys) {
    postsCount += await posts[key].count({});
  }
  const latestPosts = await getLatestPosts(posts, postsKeys, memory);
  const latestFrames = getLatestFrames(latestPosts);

  const latestFrameNames = getLatestFrameNames(latestFrames, depth, trim);
  if ((postsCount / frameCount).toFixed(2) >= threshold) {
    for (const key of postsKeys) {
      await posts[key].drop();
      await posts[key].insertMany(latestPosts[key]);
    }
    if (debug)
      console.log("Threshold met! Dropped posts for " + postsKeys.join(","));
  }
  let frame;
  let newFrame = false;
  let tries = 0;
  while (!newFrame) {
    frame = await getRandomDocument(frames);

    var foundFrames = false;
    for (const key of postsKeys) {
      const thesePosts = posts[key];
      const countedFrames = await thesePosts.countDocuments({
        frame: frame._id,
      });
      if (countedFrames > 0) {
        foundFrames = true;
      }
    }

    if (!foundFrames) {
      const name = trimName(splitFrameName(frame.name, depth), trim);
      const index = latestFrameNames.indexOf(name);
      if (debug && tries > 0) console.log(`   ${index}  > ${name}`);
      if (index === -1) {
        newFrame = true;
      }
    }
    tries++;
  }
  frame["tries"] = tries;
  return frame;
}
function getFramesWithClient(client, dbName) {
  const db = client.db(dbName);
  return getFrames(db);
}
function getFrames(db) {
  return db.collection("frames");
}
function getToots(db) {
  return db.collection("toots");
}
function getTweets(db) {
  return db.collection("tweets");
}
function getPosts(db) {
  return db.collection("posts");
}
function getTootsWithClient(client, dbName) {
  const db = client.db(dbName);
  return getToots(db);
}
function getTweetsWithClient(client, dbName) {
  const db = client.db(dbName);
  return getTweets(db);
}
function getPostsWithClient(client, dbName) {
  const db = client.db(dbName);
  return getPosts(db);
}
export async function insertPost(dbName, postType, post) {
  let client = await connectClient();
  const db = client.db(dbName);
  var postsName = "";
  switch (postType) {
    case "TWITTER":
      postsName = "tweets";
      break;
    case "MASTODON":
      postsName = "toots";
      break;
    case "BLUESKY":
      postsName = "posts";
      break;
    default:
      break;
  }
  const posts = db.collection(postsName);
  await posts.insertOne(post);
  await client.close();
  return;
}
export async function fetchFrameFromDb(profile, social, debug, client) {
  let noClient = client == null;
  if (noClient) {
    client = await connectClient();
  }
  const dbName = profile.DB_NAME;
  const frames = getFramesWithClient(client, dbName);
  var posts = {};
  for (const platform of social.split(",")) {
    switch (platform) {
      case "MASTODON":
        var toots = getTootsWithClient(client, dbName);
        posts[platform] = toots;
        break;
      case "TWITTER":
        var tweets = getTweetsWithClient(client, dbName);
        posts[platform] = tweets;
        break;
      case "BLUESKY":
        var bskys = getPostsWithClient(client, dbName);
        posts[platform] = bskys;
        break;
      default:
        throw new Error("Unrecognized social value");
    }
  }

  const frame = await getRandomFrameFromProfile(profile, frames, posts, debug);

  if (noClient) await client.close();
  return frame;
}
export const getClient = connectClient;
