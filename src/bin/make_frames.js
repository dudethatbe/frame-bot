import { getVideoFilesFromPath, entryExists, createFolder } from "../lib/videoFinder.js";
import { makeFrames } from "../lib/frameMaker.js";
const _makeFrames = async function (
  folder,
  output,
  format,
  scale,
  scaleWidth,
  scaleHeight,
  crop,
  width,
  height,
  interval,
  subtitles,
  trim,
  start,
  end,
  x,
  y,
  debug
) {
  const dumpExists = await entryExists(output);
  if (!dumpExists) {
    if (debug) {
      console.log(`Creating output folder "${output}"`);
    }
    await createFolder(output);
  }
  let files = [];
  try {
    files = await getVideoFilesFromPath(folder);
  } catch (error) {
    console.error("there was an error:", error.message);
    throw error;
  }
  if (files.length < 1) {
    console.error(`Sorry, unable to detect any video files from ${input}`);
  } else {
    // read video files
    const label = `Time to process ${files.length} files`;
    console.time(label);
    const numberOfFrames = await makeFrames(
      folder,
      files,
      output,
      format,
      scale,
      scaleWidth,
      scaleHeight,
      crop,
      width,
      height,
      x,
      y,
      interval,
      subtitles,
      trim,
      start,
      end,
      debug
    );
    console.log(
      `Created ${numberOfFrames} images from ${files.length} files (saved to: ${output})`
    );
    console.timeEnd(label);
  }
};
export { _makeFrames as makeFrames };
