import { fileIsVideo, probeVideo, getVideoStream } from "../lib/videoFinder.js";
const testFileReady = async function (testFile) {
  if (!testFile) {
    console.error("Unable to detect a file to read!");
    return;
  }
  if (!fileIsVideo(testFile)) {
    console.error(`given file "${testFile}" is not a supported media type`);
    return;
  }
  try {
    const details = await probeVideo(testFile);
    if (details) {
      if (details.streams) {
        const videoStream = getVideoStream(details);
        if (videoStream) {
          return `video stream data: ${JSON.stringify(
            videoStream
          )}. Looks good, you should be able to make frames :)`;
        } else {
          console.error(
            `Less than 1 detectable stream for "${testFile}". Unable to process this file ...`
          );
          return null;
        }
      } else {
        console.error(`Unable to detect any streams for ${testFile}`);
        return null;
      }
    } else {
      console.error(`Unable to detect any details for ${testFile}`);
      return null;
    }
  } catch (error) {
    const ffmpegNotInstalled = /bin\/sh: 1: ffprobe: not found/.test(
      error.message
    );
    if (ffmpegNotInstalled) {
      console.error(
        "looks like ffmpeg is missing; try https://ffmpeg.org/download.html"
      );
    } else {
      if (/undefined: No such file or directory/.test(error.stderr)) {
        console.error(
          "Please supply a media file to test this process (npm run ready ../Videos/funny-cat.mp4)"
        );
        return null;
      } else {
        console.error(error);
        return null;
      }
    }
  }
};
const _testFileReady = testFileReady;
export { _testFileReady as testFileReady };
