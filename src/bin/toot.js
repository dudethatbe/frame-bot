import { tryFetchProfile } from "../lib/profile.js";
import { postFrame } from "./post.js";
export async function postToot(workspace, username, debug) {
  var profile = await tryFetchProfile(workspace, username);

  if (profile.SOCIAL.indexOf("MASTODON") === -1) {
    throw Error("Unable to toot if mastodon is unconfigured");
  }

  profile.SOCIAL = "MASTODON";

  return postFrame(workspace, profile, debug);
}
