import { chooseRandomObject, downloadObject, getObjects } from "../lib/s3";
import { parse } from "path";
import { fileIsImage } from "../lib/videoFinder";
export async function downloadRandomFile(bucket, folder) {
  const object = await chooseRandomObject(bucket, folder);
  const file = await downloadObject(bucket, object.Key);
  return file;
}
export async function getFolderObjects(bucket, folder) {
  if (!folder) {
    folder = "";
    console.log(`Looking up objects in bucket ${bucket}`);
  } else {
    console.log(`Looking up files/folders uploaded to ${folder} ...`);
  }
  const label = `Time to scan ${folder}`;
  console.time(label);
  const results = await getObjects(bucket, folder);
  console.timeEnd(label);
  return results;
}

export async function getFiles(objects) {
  const files = new Set();
  for (const object of objects) {
    const key = object["Key"];
    const p = parse(key);
    const file = p.base;
    if (fileIsImage(file)) {
      files.add(file);
    }
  }
  return Array.from(files);
}
export async function getFolders(objects) {
  const folders = new Set();
  for (const object of objects) {
    const key = object["Key"];
    const p = parse(key);
    const subFolder = p.dir;
    if (subFolder) folders.add(subFolder);
  }
  return Array.from(folders);
}

export async function getSize(objects) {
  var total = 0;
  for (const object of objects) {
    const size = object["Key"];
    total += size;
  }
  return total;
}

export async function analyzeFolder(bucket, folder) {
  const results = await this.getFolderObjects(bucket, folder);
  let files = [];
  const folders = new Set();
  let folderSize = 0;
  for (const result of results) {
    let size = result["Size"];
    const key = result["Key"];
    const p = parse(key);
    const file = p.base;
    const subFolder = p.dir;
    if (fileIsImage(file)) {
      files.push(file);
    }
    folders.add(subFolder);
    folderSize += size;
  }

  const numberOfFolders = folders.size;
  const numberOfFiles = files.length;
  const size = `${(folderSize * 1e-9).toFixed(2)}GB`;

  console.log(`Number of folders\t${numberOfFolders}`);
  console.log(`Number of files\t\t${numberOfFiles}`);
  console.log(`Estimated size\t\t${size}`);
  return results;
}
