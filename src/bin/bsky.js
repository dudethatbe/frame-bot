import { tryFetchProfile } from "../lib/profile.js";
import { postFrame } from "./post.js";

export async function postBsky(workspace, username, debug) {
  var profile = await tryFetchProfile(workspace, username);

  if (profile.SOCIAL.indexOf("BLUESKY") === -1) {
    throw Error("Unable to post if bluesky is unconfigured");
  }

  profile.SOCIAL = "BLUESKY";

  return postFrame(workspace, profile, debug);
}