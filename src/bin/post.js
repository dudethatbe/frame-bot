import { chooseFileFromDb } from "../lib/cloud.js";
import { postSocialMedia, insertPost } from "../lib/social.js";
import { unlink } from "fs/promises";

export async function postFrame(workspace, profile, debug) {
  const { frame, file, key } = await chooseFileFromDb(
    workspace,
    profile,
    profile.SOCIAL,
    debug
  );
  for (const social of profile.SOCIAL.split(",")) {
    const post = await postSocialMedia(profile, social, file, key);
    await insertPost(profile.DB_NAME, social, post, frame);
  }
  console.log(new Date().toLocaleString());
  await unlink(file);
  return;
}
