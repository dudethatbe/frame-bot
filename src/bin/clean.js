import { resolve, parse } from "path";
import { getFoldersFromPath, getFoldersFromPathAndIgnore, getImagesFromPath, getImagesFromPathAndIgnore } from "../lib/videoFinder.js";
import { cleanFrames } from "../lib/clean.js";
import { mkdir, rm } from "fs/promises";

async function getTrash(folder, trash) {
  const trashCan = resolve(folder, trash);
  try {
    await mkdir(trashCan);
  } catch (e) {
    if (e.code != "EEXIST") {
      console.error("Unexpected error when creating trash can");
      throw e;
    }
  }
  return trashCan;
}
async function emptyTrash(trash) {
  await rm(trash, { recursive: true });
}
const _cleanFrames = async function (
  folder,
  percentage,
  tolerance,
  autoDelete,
  ignore,
  trash,
  attempts,
  quick,
  debug
) {
  if (!folder) {
    console.error("Unable to detect a folder to read from!");
    return;
  }

  var folders = [];
  if (ignore) {
    if (debug) console.log(`searching ${folder}; ignoring ${ignore}`);
    folders = await getFoldersFromPathAndIgnore(folder, ignore);
  } else {
    if (debug) console.log(`searching ${folder}`);
    folders = await getFoldersFromPath(folder);
  }
  if (folders.length === 0) {
    console.warn(`Unable to find any folders to process from ${folder}`);
    return;
  }

  for (let folder of folders) {
    console.time(folder);
    let trashCan = await getTrash(folder, trash);
    let imageFiles;

    if (ignore) {
      imageFiles = await getImagesFromPathAndIgnore(folder, ignore);
    } else {
      imageFiles = await getImagesFromPath(folder);
    }

    await cleanFrames(
      imageFiles,
      trashCan,
      percentage,
      tolerance,
      attempts,
      quick,
      debug
    );
    if (debug) console.log(`deleting trash ${trashCan}? (${autoDelete})`);
    if (autoDelete) {
      await emptyTrash(trashCan);
    }
    console.timeEnd(folder);
  }
};
export { _cleanFrames as cleanFrames };
