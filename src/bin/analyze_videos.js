import { getVideoFilesFromPath } from "../lib/videoFinder.js";
import { analyzeVideos } from "../lib/frameMaker.js";

export const analyzeVideosBin = async function (readFrom, writeTo) {
  if (!readFrom) {
    console.error("Unable to detect a folder to read from!");
    return;
  }
  if (!writeTo) {
    writeTo = "./results.csv";
  }
  const videos = await getVideoFilesFromPath(readFrom);
  return analyzeVideos(videos, writeTo);
};