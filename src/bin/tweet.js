import { tryFetchProfile } from "../lib/profile.js";
import { postFrame } from "./post.js";
export async function postTweet(workspace, username, debug) {
  var profile = await tryFetchProfile(workspace, username);

  if (profile.SOCIAL.indexOf("TWITTER") === -1) {
    throw Error("Unable to tweet if twitter is unconfigured");
  }

  profile.SOCIAL = "TWITTER";

  return postFrame(workspace, profile, debug);
}
