import { countUploads } from "../lib/dropbox.js";
const analyzeDropboxUploads = async function (folder) {
  console.log(`Looking up files/folders uploaded to ${folder} ...`);
  const label = `Time to scan ${folder}`;
  console.time(label);
  const result = await countUploads(folder);
  console.timeEnd(label);
  const numberOfFolders = result.folders.length;
  const numberOfFiles = result.files.length;
  const size = `${(result.size * 1e-9).toFixed(2)}GB`;

  console.log(`Number of folders\t${numberOfFolders}`);
  console.log(`Number of files\t\t${numberOfFiles}`);
  console.log(`Estimated size\t\t${size}`);
  return result;
};
const _analyzeDropboxUploads = analyzeDropboxUploads;
export { _analyzeDropboxUploads as analyzeDropboxUploads };
