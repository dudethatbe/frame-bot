import { getImagesFromPath } from "../lib/videoFinder.js";
import { analyzeFrames } from "../lib/frameMaker.js";

const analyzeFramesBin = async function (readFrom, writeTo) {
  if (!readFrom) {
    console.error("Unable to detect a folder to read from!");
    return;
  }
  if (!writeTo) {
    writeTo = "./results.csv";
  }
  const images = await getImagesFromPath(readFrom);
  return analyzeFrames(images, writeTo);
};
const _analyzeFrames = analyzeFramesBin;
export { _analyzeFrames as analyzeFrames };
