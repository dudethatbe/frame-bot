import "dotenv/config";
import { createParser } from "dashdash";
import { groups, args } from "./const/settings.js";
const options = [
  groups.pathOptions,
  args.workspace,
  groups.profileOptions,
  args.name,
  groups.emptyOptions,
  args.help,
];
import { updateMongoDB } from "./bin/populate_mongo.js";
import { readProfile } from "./lib/profile.js";
import { getObjects as getObjectsS3, init as initS3 } from "./lib/s3.js";
import { init as initDBX, countUploads as countUploadsDBX } from "./lib/dropbox.js"
(async () => {
  const parser = createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("mongo.js: %s", e.message);
  }
  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log(
      "usage: npm run populate-mongo --  [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }
  if (!opts.name) {
    console.error("Missing profile '--name name'");
    return;
  }
  const profile = await readProfile(opts.workspace, opts.name);
  if (!profile) {
    console.error("Unable to find profile with username " + opts.profile);
    return;
  }

  console.time("time to fetch frames");
  console.log(`getting frames from ${profile.FOLDER} ...`);
  let frames;
  switch (profile.SOURCE) {
    case "S3":
      const key = "AWS_SECRET_ACCESS_KEY";
      const id = "AWS_ACCESS_KEY_ID";
      const endpoint = "ENDPOINT";
      const region = "REGION";
      initS3(profile[id], profile[key], profile[endpoint], profile[region]);
      const objects = await getObjectsS3(profile.BUCKET, profile.FOLDER);
      console.timeEnd("time to fetch frames");
      frames = objects
        .map((obj) => {
          return {
            name: obj.Key,
            size: obj.Size,
            eTag: obj.ETag,
            rnd: Math.random(),
          };
        })
        .filter((frame) => frame.size > 0);
      break;
    case "DBX":
      initDBX(profile["DBX_ACCESS_TOKEN"]);
      var { files } = await countUploadsDBX(profile["FOLDER"]);
      frames = files.map((f) => {
        return {
          name: f,
          rnd: Math.random(),
        };
      });
      break;
    default:
      throw new Error(`Unable to read SOURCE ${profile.SOURCE}`);
  }

  await updateMongoDB(profile.DB_NAME, frames);
})();
