import "dotenv/config";
import { createParser } from "dashdash";
import { groups, args } from "./const/settings.js";
import { postBsky } from "./bin/bsky.js";

const options = [
  groups.pathOptions,
  args.workspace,
  groups.profileOptions,
  args.name,
  args.help,
  groups.emptyOptions,
  args.debug,
];

(async function () {
  const parser = createParser({ options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("toot.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log("usage: npm run toot --  [OPTIONS]\n" + "options:\n" + help);
    return;
  }

  if (opts.name) {
    await postBsky(opts.workspace, opts.name, opts.debug);
  }
})();