import "dotenv/config";
import { analyzeDropboxUploads } from "./bin/analyze_dropbox_uploads.js";

(async function (inputFolder) {
  let folder = inputFolder || process.env.FOLDER;
  if (!folder) {
    throw new Error(
      "unable to detect FOLDER set in .env or through the command-line"
    );
  }
  if (!folder.startsWith("/")) folder = `/${folder}`;

  console.log(await analyzeDropboxUploads(folder));
})(process.argv[2]);
