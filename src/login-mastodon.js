import "dotenv/config";
import { Strategy } from "passport-mastodon";
import { use, serializeUser, deserializeUser, initialize, session as _session, authenticate } from "passport";
import express from "express";
import session from "express-session";
import { updateProfileMastodon } from "./lib/profile";
const app = express();
const port = process.env.PORT;
const { MASTODON_CLIENT_KEY, MASTODON_CLIENT_SECRET, CALLBACK_URL, DOMAIN } = process.env;
if (MASTODON_CLIENT_KEY && MASTODON_CLIENT_SECRET && CALLBACK_URL && DOMAIN) {
  use(
    new Strategy(
      {
        clientID: MASTODON_CLIENT_KEY,
        clientSecret: MASTODON_CLIENT_SECRET,
        domain: DOMAIN,
        callbackURL: CALLBACK_URL,
      },
      (accessToken, refreshToken, profile, cb) => {
        const response = {
          MASTODON_ACCESS_TOKEN: accessToken,
          profile: profile,
        };
        return cb(null, response);
      }
    )
  );
} else {
  throw new Error("Unable to detect consumer key, secret, and callback_url");
}
serializeUser(function (user, done) {
  done(null, user);
});
deserializeUser(function (user, done) {
  done(null, user);
});
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
  })
);
app.use(initialize());
app.use(_session());

app.get("/", (req, res) => {
  res.send({
    message: `go to "http://localhost:${port}/login/mastodon" to open the OAuth login page`,
  });
});
app.get("/login/mastodon", authenticate("mastodon"));
app.get(
  "/login/mastodon/return",
  authenticate("mastodon", { failureRedirect: "/login" }),
  async function (req, res) {
    res.send({ user: req.user });
    const username = req.user.profile.username;
    const location = process.cwd();
    await updateProfileMastodon(
      location,
      username,
      req.user,
      CLIENT_KEY,
      CLIENT_SECRET
    );
  }
);
app.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/");
});
app.listen(port, () => {
  console.log(
    `waiting to authenticate Mastodon login on port localhost:${port}/login/mastodon`
  );
});
export default app;
