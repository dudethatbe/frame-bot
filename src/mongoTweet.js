import "dotenv/config";
import { createParser } from "dashdash";
import { groups, args } from "./const/settings.js";
const options = [
  groups.pathOptions,
  args.workspace,
  groups.profileOptions,
  args.name,
  groups.dbOptions,
  args.stress,
  groups.choiceOptions,
  args.tweetThreshold,
  args.tweetMemory,
  args.tweetDepth,
  args.tweetTrimName,
  groups.emptyOptions,
  args.debug,
  args.help,
];
import { getFrameAndTweetCounts, getClient, fetchFrameFromDb } from "./bin/tweet_mongo.js";
import { fetchProfile } from "./lib/profile.js";
(async () => {
  const parser = createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("mongoTweetjs: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true });
    console.log(
      "usage: npm run populate-mongo --  [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }

  if (!opts.name) {
    console.error("Unable to detect a profile name '--name profilename'");
    return;
  }
  const profile = await fetchProfile(opts.name);

  const source = profile["SOURCE"];

  switch (source) {
    case "S3":
    case "DBX":
      break;
    default:
      throw new Error(`Unrecognized SOURCE ${source}`);
  }

  if (opts.stress) {
    console.time("stress");
    var client = await getClient();
    const { framesCount, tweetsCount } = await getFrameAndTweetCounts(
      profile["DB_NAME"],
      client
    );
    console.log(`${tweetsCount} tweets made; ${framesCount} frames in db`);
    // tweets however times it takes to go one over the edge
    const totalTweets = framesCount - tweetsCount + 1;
    console.log(`simulating ${totalTweets} tweets;`);

    for (let i = 0; i <= totalTweets; i++) {
      const frame = await fetchFrameFromDb(profile, opts.debug, client);
      const percentage = parseFloat(((i / totalTweets) * 100).toFixed(2))
        .toFixed()
        .padStart(3);
      console.log(`${percentage}%`, frame.name);
    }
    await client.close();
    console.timeEnd("stress");
  }
})();
