function toTwoDigitNum(value) {
  return value.toFixed(2) * 100;
}
export function floatToPercentStr(value) {
  return toTwoDigitNum(value).toFixed(0) + "%";
}
export function floatToNum(value) {
  return toTwoDigitNum(value);
}
