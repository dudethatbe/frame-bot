{
  "groups": {
    "pathOptions": { "group": "Path Options" },
    "cleanOptions": { "group": "Clean Options" },
    "frameOptions": { "group": "Frame Options" },
    "cloudOptions": { "group": "Cloud Options" },
    "twitterOptions": { "group": "Twitter Options" },
    "scalingOptions": { "group": "Scaling Options" },
    "croppingOptions": { "group": "Cropping Options" },
    "herokuOptions": { "group": "Heroku Options" },
    "scriptOptions": { "group": "Script Options" },
    "dbOptions": { "group": "Database Options" },
    "choiceOptions": { "group": "Choice Options" },
    "profileOptions": { "group": "Profile Options" },
    "emptyOptions": { "group": "" }
  },
  "args": {
    "folder": {
      "names": ["folder", "f"],
      "type": "string",
      "env": "FOLDER",
      "help": "Name of folder to read videos from, or select a frame from",
      "helpArg": "PATH"
    },
    "output": {
      "names": ["output", "o"],
      "type": "string",
      "help": "Name of folder to save frames to",
      "helpArg": "PATH"
    },
    "results": {
      "name": "results",
      "type": "string",
      "help": "File path of where to save .csv file to",
      "helpArg": "PATH",
      "default": "results.csv"
    },
    "script": {
      "name": "script",
      "type": "string",
      "help": "File path of where to save .sh script file to",
      "helpArg": "PATH",
      "default": "heroku.sh"
    },
    "interval": {
      "names": ["interval", "n"],
      "type": "integer",
      "env": "FRAME_INTERVAL",
      "help": "How often to render a frame from video",
      "helpArg": "N"
    },
    "subtitles": {
      "name": "subtitles",
      "type": "bool",
      "help": "Find associated subtitle/caption files and include in video"
    },
    "trim": {
      "name": "trim",
      "type": "bool",
      "help": "Trim seconds from the input video"
    },
    "start": {
      "name": "start",
      "type": "integer",
      "help": "Number of seconds for video, or characters in path name (from beginning)"
    },
    "end": {
      "name": "end",
      "type": "integer",
      "help": "Number of seconds for video, or characters in path name (going backwards from the end)"
    },
    "width": {
      "names": ["width", "w"],
      "type": "integer",
      "env": "FRAME_WIDTH",
      "help": "Width of frames in pixels",
      "helpArg": "PIXELS"
    },
    "height": {
      "names": ["height", "h"],
      "type": "integer",
      "env": "FRAME_HEIGHT",
      "help": "Height of frames in pixels",
      "helpArg": "PIXELS"
    },
    "x": {
      "name": "x",
      "type": "integer",
      "help": "X-position of where to crop from",
      "helpArg": "PIXELS"
    },
    "y": {
      "name": "y",
      "type": "integer",
      "help": "Y-position of where to crop to. Defaults to FRAME_WIDTH or video width",
      "helpArg": "PIXELS"
    },
    "scale": {
      "name": "scale",
      "type": "bool",
      "default": false,
      "env": "SCALE",
      "help": "Scales the frame dimensions; Use with --width & --height"
    },
    "format": {
      "name": "format",
      "type": "string",
      "help": "File Format for the frames",
      "helpArg": "FORMAT",
      "default": "jpg"
    },
    "luminanceTolerance": {
      "names": ["tolerance", "t"],
      "type": "string",
      "help": "Maximum luminance tolerance \"t\"; Any values greater than t are ignored",
      "helpArg": "0.N",
      "default": "0.03",
      "env": "TOLERANCE"
    },
    "luminancePercentage": {
      "names": ["percentage", "p"],
      "type": "string",
      "help": "Minimum percentage of dominant luminance \"p\"; Any values less than p are ignored",
      "helpArg": "0.N",
      "default": "0.9",
      "env": "PERCENTAGE"
    },
    "deleteTrash": {
      "name": "delete",
      "type": "bool",
      "help": "Automatically delete the frames for cleaning",
      "default": false
    },
    "ignorePath": {
      "name": "ignore",
      "type": "string",
      "help": "Ignore a part of a path from file/folder finding",
      "default": "trash",
      "env": "IGNORE"
    },
    "slice": {
      "name": "slice",
      "type": "bool",
      "help": "Remove characters from the frame names for comparison"
    },
    "trashPath": {
      "name": "trash",
      "type": "string",
      "help": "Name of folder to place garbage files temporarily",
      "default": "trash",
      "env": "TRASH"
    },
    "attempts": {
      "name": "attempts",
      "type": "integer",
      "help": "Number of files to look at before moving on to the next one",
      "default": 10,
      "env": "ATTEMPTS"
    },
    "quickSort": {
      "name": "quick",
      "type": "bool",
      "help": "Sort files by size while cleaning instead of cleaning beginning and end",
      "default": true
    },
    "crop": {
      "name": "crop",
      "type": "bool",
      "env": "CROP",
      "help": "Crop the frames by using x, y, width, and height parameters (pixels)",
      "default": false
    },
    "scaleWidth": {
      "name": "scaleWidth",
      "type": "integer",
      "env": "SCALE_WIDTH",
      "help": "Set the width of the scaled frames; defaults to the width of the video if not specified"
    },
    "scaleHeight": {
      "name": "scaleHeight",
      "type": "integer",
      "env": "SCALE_HEIGHT",
      "help": "Set the height of the scaled frames; defaults to the height of the video if not specified"
    },
    "workspace": {
      "name": "workspace",
      "type": "string",
      "default": "./",
      "help": "Where to save the downloaded file to",
      "helpArg": "PATH"
    },
    "source": {
      "name": "source",
      "type": "string",
      "env": "SOURCE",
      "default": "s3",
      "help": "Remote hosting option",
      "helpArg": "S3 or DBX"
    },
    "dbHost": {
      "name": "dbHost",
      "type": "string",
      "env": "DB_HOST",
      "help": "Host location for DB"
    },
    "dbName": {
      "name": "dbName",
      "type": "string",
      "env": "DB_NAME",
      "help": "Name of database to connect to"
    },
    "dbUser": {
      "name": "dbUser",
      "type": "string",
      "env": "DB_USER",
      "help": "User name for DB auth"
    },
    "dbPass": {
      "name": "dbPass",
      "type": "string",
      "env": "DB_PASS",
      "help": "Password for DB auth"
    },
    "stress": {
      "name": "stress",
      "type": "bool",
      "default": false,
      "help": "Stress test the mongo db selection"
    },
    "tweetThreshold": {
      "name": "tweetThreshold",
      "type": "string",
      "default": 0.7,
      "help": "The tweets/frames ratio that determines when the tweets collection is reset",
      "env": "TWEET_THRESHOLD"
    },
    "tweetMemory": {
      "name": "tweetMemory",
      "type": "integer",
      "default": 10,
      "help": "Number of most recently tweeted frames to avoid duplicating",
      "env": "TWEET_MEMORY"
    },
    "tweetDepth": {
      "name": "tweetDepth",
      "type": "integer",
      "default": 1,
      "help": "Depth to select after splitting by '/'",
      "env": "TWEET_DEPTH"
    },
    "tweetTrimName": {
      "name": "tweetTrimName",
      "type": "integer",
      "default": 10,
      "help": "Trims a number of characters N from the beginning of the name of the file path",
      "env": "TWEET_TRIM_NAME",
      "helpArg": "N"
    },
    "awsBucket": {
      "name": "bucket",
      "type": "string",
      "env": "BUCKET"
    },
    "awsEndpoint": {
      "name": "endpoint",
      "type": "string",
      "env": "ENDPOINT"
    },
    "awsRegion": {
      "name": "region",
      "type": "string",
      "env": "REGION"
    },
    "dbxAccessToken": {
      "name": "dbx-access-token",
      "type": "string",
      "env": "DBX_ACCESS_TOKEN"
    },
    "awsAccessKeyId": {
      "name": "aws-access-key",
      "type": "string",
      "env": "AWS_ACCESS_KEY_ID"
    },
    "awsSecretAccessKey": {
      "name": "aws-secret-key",
      "type": "string",
      "env": "AWS_SECRET_ACCESS_KEY"
    },
    "twitterConsumerKey": {
      "name": "consumer-key",
      "type": "string",
      "env": "TWITTER_CONSUMER_KEY"
    },
    "twitterConsumerSecret": {
      "name": "consumer-secret",
      "type": "string",
      "env": "TWITTER_CONSUMER_SECRET"
    },
    "mastodonConsumerKey": {
      "name": "consumer-key",
      "type": "string",
      "env": "MASTODON_CONSUMER_KEY"
    },
    "mastodonConsumerSecret": {
      "name": "consumer-secret",
      "type": "string",
      "env": "MASTODON_CONSUMER_SECRET"
    },
    "twitterAccessTokenKey": {
      "name": "access-token-key",
      "type": "string",
      "env": "TWITTER_ACCESS_TOKEN_KEY",
      "help": "Twitter user access token key"
    },
    "twitterAccessTokenSecret": {
      "name": "access-token-secret",
      "type": "string",
      "env": "TWITTER_ACCESS_TOKEN_SECRET",
      "help": "Twitter user access token secret"
    },
    "mastodonAccessToken": {
      "name": "access-token",
      "type": "string",
      "env": "MASTODON_ACCESS_TOKEN_KEY",
      "help": "Mastodon user access token"
    },
    "herokuAppName": {
      "name": "heroku-app-name",
      "type": "string",
      "env": "HEROKU_APP_NAME"
    },
    "redirectUrl": {
      "name": "redirect-url",
      "type": "string",
      "env": "REDIRECT_URL",
      "help": "Twitter page to redirect to"
    },
    "unsetDbx": {
      "name": "unset-dbx",
      "type": "bool",
      "help": "Remove dropbox settings from heroku app"
    },
    "name": {
      "name": "name",
      "type": "string",
      "help": "Name of the profile to update"
    },
    "autoRun": {
      "name": "autoRun",
      "type": "bool",
      "help": "Detect and apply all settings from .env to profile"
    },
    "formatProfile": {
      "name": "formatProfile",
      "type": "bool",
      "help": "Clear all settings from profile before applying from .env"
    },
    "setS3": {
      "name": "setS3",
      "type": "bool",
      "help": "Update profile with S3 settings from .env"
    },
    "setDbx": {
      "name": "setDbx",
      "type": "bool",
      "help": "Update profile with Dropbox token from .env"
    },
    "setSocial": {
      "name": "setSocial",
      "type": "bool",
      "help": "Apply the SOCIAL settings from .env to profile"
    },
    "setSource": {
      "name": "setSource",
      "type": "string",
      "help": "Change the SOURCE in the profile to S3 or DBX"
    },
    "setDb": {
      "name": "setDb",
      "type": "bool",
      "help": "Update profile with database settings from .env"
    },
    "setFolder": {
      "name": "setFolder",
      "type": "string",
      "help": "Change the FOLDER in the profile to a specified location"
    },
    "setDbName": {
      "name": "setDbName",
      "type": "string",
      "help": "Change the DB_NAME in the profile to select a database"
    },
    "setTweet": {
      "name": "setTweet",
      "type": "bool",
      "help": "Update the tweet choice settings from .env"
    },
    "setToot": {
      "name": "setToot",
      "type": "bool",
      "help": "Set the Mastodon settings from .env to the profile"
    },
    "setInterval": {
      "name": "setInterval",
      "type": "bool",
      "help": "Save the frame interval from .env to the profile"
    },
    "validate": {
      "name": "validate",
      "type": "bool",
      "help": "Read profile and validate it"
    },
    "sync": {
      "name": "sync",
      "type": "bool",
      "default": false,
      "help": "Save the profile to the database"
    },
    "download": {
      "name": "download",
      "type": "bool",
      "default": false,
      "help": "Download the profile after fetching from db"
    },
    "init": {
      "name": "init",
      "type": "bool",
      "default": false,
      "help": "Initialize a new profile by creating it locally"
    },
    "debug": {
      "name": "debug",
      "type": "bool",
      "help": "show additional messages in terminal"
    },
    "help": {
      "name": "help",
      "type": "bool",
      "help": "show the help text"
    }
  }
}
