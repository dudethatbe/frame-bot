import { testFileReady } from "./bin/check_if_ready.js";
(async function (testFile) {
  console.log(await testFileReady(testFile));
})(process.argv[2]);
