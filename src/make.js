import "dotenv/config";
import { createParser } from "dashdash";
import { groups, args } from "./const/settings.js";
import { makeFrames } from "./bin/make_frames.js";
const options = [
  groups.pathOptions,
  args.folder,
  args.output,
  args.format,
  groups.frameOptions,
  args.interval,
  args.subtitles,
  args.trim,
  args.start,
  args.end,
  groups.croppingOptions,
  args.crop,
  args.width,
  args.height,
  args.x,
  args.y,
  groups.scalingOptions,
  args.scale,
  args.scaleWidth,
  args.scaleHeight,
  groups.emptyOptions,
  args.debug,
  args.help,
];

(async function () {
  const parser = createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("make.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true }).trimRight();
    console.log("usage: npm run make-frames [OPTIONS]\n" + "options:\n" + help);
    return;
  }

  if (!opts.folder) {
    console.error("Folder is missing '-i INPUT'");
    return;
  }

  if (!opts.output) {
    console.error("Output folder is missing '-o OUTPUT'");
    return;
  }

  if (!opts.interval) {
    console.error("Unable to detect interval '-n INTERVAL'");
    return;
  }

  if (opts.interval <= 0) {
    console.error("Interval must be greater than 0");
    return;
  }
  if (opts.crop && !opts.width && !opts.height && !opts.x && !opts.y) {
    console.error(
      "Cropping option present, but no additional data given (-w, -h, -x, and -y are empty"
    );
    return;
  }

  if (opts.scale && !opts.scaleWidth && !opts.scaleHeight) {
    console.error(
      "Scaling option present, but neither --scaleWidth or --scaleHeight given"
    );
    return;
  }

  if (opts.trim) {
    if (!opts.start && !opts.end) {
      console.error(
        "Trimming option given, but there's no usage of --start or --end"
      );
      return;
    }
    if (opts.start < 0 || opts.end < 0) {
      console.error("Start or End trim cannot be less than 0");
      return;
    }
  }

  makeFrames(
    opts.folder,
    opts.output,
    opts.format,
    opts.scale,
    opts.scaleWidth,
    opts.scaleHeight,
    opts.crop,
    opts.width,
    opts.height,
    opts.interval,
    opts.subtitles,
    opts.trim,
    opts.start,
    opts.end,
    opts.x,
    opts.y,
    opts.debug
  );
})();
