import "dotenv/config";
import { createParser } from "dashdash";
import { groups, args } from "./const/settings.js";
import { analyzeFolder } from "./bin/s3.js";
const options = [
  groups.pathOptions,
  args.folder,
  args.workspace,
  groups.cloudOptions,
  args.bucket,
  args.endpoint,
  args.region,
  args.awsAccessKeyId,
  args.awsSecretAccessKey,
  groups.emptyOptions,
  args.help,
];

(async function () {
  const parser = createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("s3.js: %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true });
    console.log(
      "usage: npm run analyze-s3 -- [OPTIONS]\n" + "options:\n" + help
    );
    return;
  }

  await analyzeFolder(opts.bucket, opts.folder);
})();
