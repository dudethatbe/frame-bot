import "dotenv/config";
import { createParser } from "dashdash";
import { groups, args } from "./const/settings.js";
import { postFrame } from "./bin/post.js";
import { tryFetchProfile } from "./lib/profile.js";

const options = [
  groups.pathOptions,
  args.workspace,
  groups.profileOptions,
  args.name,
  groups.emptyOptions,
  args.help,
  args.debug,
];

(async function () {
  const parser = createParser({ options: options });
  try {
    var opts = parser.parse(process.argv);
  } catch (e) {
    console.error("post.js %s", e.message);
  }

  if (opts.help) {
    const help = parser.help({ includeEnv: true });
    console.log("usage: npm run post -- [OPTIONS]\n" + "options:\n" + help);
    return;
  }

  if (opts.name) {
    var profile = await tryFetchProfile(opts.workspace, opts.name);
    if (!profile) {
      console.error("Unable to retrieve profile");
      return;
    }
    await postFrame(opts.workspace, profile, opts.debug);
  }
})();
