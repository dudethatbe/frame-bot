import "./const/config.js"
import { Strategy } from "passport-twitter";
import passport from "passport";
import express from "express";
import session from "express-session";
import { updateProfileTwitter } from "./lib/profile.js";
const app = express();
const port = process.env.PORT;
const { TWITTER_CONSUMER_KEY, CONSUMER_KEY, TWITTER_CONSUMER_SECRET, CONSUMER_SECRET, CALLBACK_URL, PORT } = process.env;
if ((TWITTER_CONSUMER_KEY || CONSUMER_KEY) && (TWITTER_CONSUMER_SECRET || CONSUMER_SECRET) && CALLBACK_URL && PORT) {
  passport.use(
    new Strategy(
      {
        consumerKey: TWITTER_CONSUMER_KEY ? TWITTER_CONSUMER_KEY : CONSUMER_KEY,
        consumerSecret: TWITTER_CONSUMER_SECRET ? TWITTER_CONSUMER_SECRET : CONSUMER_SECRET,
        callbackURL: CALLBACK_URL,
      },
      (token, tokenSecret, profile, cb) => {
        const response = {
          ACCESS_TOKEN_KEY: token,
          ACCESS_TOKEN_SECRET: tokenSecret,
          username: profile.username,
          photos: profile.photos,
        };

        return cb(null, response);
      }
    )
  );
} else {
  throw new Error("Unable to detect consumer key, secret, and callback_url");
}
passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (user, done) {
  done(null, user);
});
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(session());

app.get("/", (req, res) => {
  res.send({
    message: `go to "http://localhost:${port}/login/twitter" to open the OAuth login page`,
  });
});
app.get("/login/twitter", passport.authenticate("twitter"));
app.get(
  "/login/twitter/return",
  passport.authenticate("twitter", { failureRedirect: "/login" }),
  async function (req, res) {
    const username = req.user.username;
    const location = process.cwd();

    const oldVars = CONSUMER_KEY && CONSUMER_SECRET;
    if (oldVars) {
      req.user["CONSUMER_KEY"] = CONSUMER_KEY;
      req.user["CONSUMER_SECRET"] = CONSUMER_SECRET;
    } else {
      req.user["CONSUMER_KEY"] = TWITTER_CONSUMER_KEY;
      req.user["CONSUMER_SECRET"] = TWITTER_CONSUMER_SECRET;
    }
    await updateProfileTwitter(location, username, req.user, oldVars)
    res.send({ user: req.user });
  }
);
app.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/");
});
app.listen(port, () => {
  console.log(
    `waiting to authenticate Twitter login on port localhost:${port}/login/twitter`
  );
});
export default app;
