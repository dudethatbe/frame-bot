# Requirements

- FFmpeg
  - https://ffmpeg.org/download.html
- Node.js 18.x
  - https://nodejs.org/en/download/

# About

This project was designed for a single-user to manage several bots that post images on social media. Instead of running an application, the approach here is entirely script-based. This means every action has a script associated with it.

This README is a living document and may not always be up to date. Feel free to create an issue with your question if you need any help using this project.

# Features

These scripts allow you to make a funny image bot by performing these tasks

 * Extract images from videos
   * Crop & scale output images
   * Variable interval for frame selection
   * Trim seconds from beginning and end of videos
   * Include subtitles (.srt files)
 * Video and image analysis
   * Detect dark images and remove them
   * Display metadata for several videos in a directory
* Post images to social media
   * Post to several platforms at once
   * Keep track of previously selected images


# Getting Started

  Browse the [wiki](https://gitlab.com/dudethatbe/frame-bot/-/wikis/home) for getting started and further configuration 📚 

# History

The first "frame-bot" project can be found here: https://github.com/dudethatbe/frame-bot
