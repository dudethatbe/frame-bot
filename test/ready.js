const { describe } = require("mocha");
const { testFileFullPath } = require("./assets/paths.json");
const { equal } = require("chai").assert;
const script = require("../src/bin/check_if_ready");
describe("FFmpeg Availability", function () {
  describe("#testFileReady", function () {
    it("should be able to test whether FFmpeg is installed and available in the PATH", async function () {
      const results = await script.testFileReady(testFileFullPath);
      return equal(
        results.startsWith("video stream data: {"),
        true,
        "expected script to detect a video stream from the test file"
      );
    });
  });
});
