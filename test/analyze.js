const { testPathToAssets, testFileCSV } = require("./assets/paths.json");
const { describe } = require("mocha");
const { equal } = require("chai").assert;

const script = require("../src/bin/analyze_videos");

describe("Video Analysis", function () {
  describe("#analyzeVideos()", function () {
    it("should be able to analyze a folder of videos", async function () {
      const results = await script.analyzeVideos(testPathToAssets, testFileCSV);
      return equal(results.length, 1, "expected 2 entries to be in assets");
    });
  });
});
