const path = require("path");
const { describe } = require("mocha");
const { equal, isAbove } = require("chai").assert;
const {
  testPathToAssets,
  testFileName,
  testFileFullPath,
  testFileNotVideo,
  testFolderNotExist,
  testFolderToCreate
} = require("../assets/paths.json");
const finder = require("../../src/lib/videoFinder");

describe("VideoFinder", function () {
  describe("#fileIsVideo()", function () {
    it("should be able to recognize a video file", function (done) {
      equal(
        finder.fileIsVideo(testFileFullPath),
        true,
        "expected filename to be detected as a video file"
      );
      done();
    });
    it("should be able to reject a file that isn't a video", function (done) {
      equal(
        finder.fileIsVideo(testFileNotVideo),
        false,
        "expected filename to be *not* be detected as a video file"
      );
      done();
    });
  });
  describe("#folderExists()", function () {
    it("should be able to accept a valid folder path", async function () {
      const folderExists = await finder.folderExists(testPathToAssets);
      return equal(
        folderExists,
        true,
        "expected an assets folder to exist in test folder"
      );
    });
    it("should be able to determine when something isn't a folder", async function () {
      const folderExists = await finder.folderExists(testFolderNotExist);
      return equal(
        folderExists,
        false,
        "did not expect a warner bro's media folder to exist in the assets folder"
      );
    });
  });
  describe("#getVideoFilesFromPath()", function () {
    it("should be able to detect one video file from the assets folder", async function () {
      const files = await finder.getVideoFilesFromPath(testPathToAssets);
      return Promise.all([
        equal(files.length, 1, "expected one file from the assets folder"),
        equal(
          path.basename(files[0]),
          testFileName,
          "expected to find a video file"
        )
      ]);
    });
    it("should be able to handle a non-existant folder", async function () {
      const files = await finder.getVideoFilesFromPath(testFolderNotExist);
      return equal(files.length, 0, "expected files array to be empty");
    });
  });
  describe("#createFolder()", function () {
    it("should be able to create a folder", async function () {
      const createdFolder = await finder.createFolder(testFolderToCreate);
      await finder.deleteFolder(testFolderToCreate);
      return equal(
        createdFolder,
        true,
        "expected function to return true after creating the folder"
      );
    });
  });
  describe("#getVideoStream()", function () {
    it("should be able to detect a video stream from a video file", async function () {
      const details = await finder.probeVideo(testFileFullPath);
      const videoStream = await finder.getVideoStream(details.streams);
      /* console.log(videoStream);
      {
        width: 240,
        height: 180,
        duration: 149,
        disposition: {
          default: 1,
          dub: 0,
          ...
        },
        tags: { language: 'und', handler_name: 'VideoHandler' }
      } */
      return Promise.all([
        isAbove(
          videoStream.duration,
          140,
          "expected test file to be greater than 140 seconds"
        ),
        equal(
          videoStream.width,
          240,
          "expected test video width to be 240 pixels"
        ),
        equal(
          videoStream.height,
          180,
          "expected test video height to be 180 pixels"
        )
      ]);
    });
  });
  describe("#probeVideo()", function () {
    it("should be able to probe a video file", async function () {
      const details = await finder.probeVideo(testFileFullPath);
      /* console.log(details);
      {
        programs: [],
        streams: [
          {
            width: 240,
            height: 180,
            duration: '149.000000',
            disposition: [Object],
            tags: [Object]
          },
          { duration: '149.141769', disposition: [Object], tags: [Object] }
        ]
      } */

      return equal(
        details.streams.length,
        2,
        "expected the test file to have two streams"
      );
    });
  });
});
