const { access, rm } = require("fs").promises;
const { describe } = require("mocha");
const { equal } = require("chai").assert;
const {
  testPathToAssets,
  testFolderForFrames,
  testFileName,
  testFileCSV
} = require("../assets/paths.json");
const {
  getVideoFilesFromPath,
  createFolder
} = require("../../src/lib/videoFinder");
const { analyzeVideos, makeFrames } = require("../../src/lib/frameMaker");

describe("FrameMaker", function () {
  describe("#analyzeVideos()", function () {
    it("should be able to analyze a folder of videos", async function () {
      try {
        await rm(testFileCSV);
      } catch (e) {
        // ignore error if the file doesn't exist
        if (e.code !== "ENOENT") console.error(e);
      }
      const files = await getVideoFilesFromPath(testPathToAssets);
      const results = await analyzeVideos(files, testFileCSV);
      const result = results[0];
      let csvFileExists = false;
      try {
        await access(testFileCSV);
        csvFileExists = true;
      } catch (e) {
        console.error(e);
      }
      return Promise.all([
        equal(csvFileExists, true, "expected a CSV file to exist"),
        equal(results.length, 1, "expected one file to be in the results"),
        equal(result.width, 240, "expected file width to be 240"),
        equal(
          result.file.endsWith(testFileName),
          true,
          "expected file result to be the same video file"
        )
      ]);
    });
  });
  describe("#makeFrames()", function () {
    it("should be able to generate frames from a video to a folder", async function () {
      if (
        !process.env.FRAME_HEIGHT ||
        !process.env.FRAME_WIDTH ||
        !process.env.FRAME_INTERVAL
      ) {
        this.skip();
      } else {
        const files = await getVideoFilesFromPath(testPathToAssets);
        await createFolder(testFolderForFrames);
        const framesCount = await makeFrames(files, testFolderForFrames);
        return equal(framesCount, 2, "expected number of frames to be 2");
      }
    });
  });
});
