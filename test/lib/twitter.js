const { equal, isAbove } = require("chai").assert;
const { chooseRandomFrame, downloadFile } = require("../../src/lib/dropbox");
const { postTweet } = require("../../src/lib/twitter");
const tweeter = require("../../src/tweet");
const { describe } = require("mocha");
const {
  testFolderForDownloads,
  dropboxFolderNotExists,
  dropboxFolderExists
} = require("../assets/paths.json");
describe("Twitter", function () {
  it("should be able to post a tweet", async function () {
    this.timeout(10000);
    const {
      ACCESS_TOKEN,
      ACCESS_TOKEN_KEY,
      ACCESS_TOKEN_SECRET,
      CONSUMER_KEY,
      CONSUMER_SECRET
    } = process.env;
    if (
      !ACCESS_TOKEN ||
      !ACCESS_TOKEN_KEY ||
      !ACCESS_TOKEN_SECRET ||
      !CONSUMER_KEY ||
      !CONSUMER_SECRET
    ) {
      this.skip();
    } else {
      const folder = dropboxFolderExists;
      console.log(`Choosing a file to download from ${folder} ...`);

      const randomFrame = await chooseRandomFrame(folder);
      const path = randomFrame.path_lower;
      const { file, response } = await downloadFile(
        path,
        testFolderForDownloads
      );
      console.log(`Downloaded file to ${file}`);

      const tweet = await postTweet(file);
      const tweetTime = new Date(tweet.created_at);
      const today = new Date();
      console.log(`${tweetTime} => ${tweet.text}`);

      return Promise.all([
        equal(response.status, 200, "expected status code to be 200"),
        isAbove(
          response.result.size,
          50,
          "expected size of file to be greater than 50 bytes"
        ),
        equal(
          /^https/.test(tweet.text),
          true,
          `expected tweet text to start with https`
        ),
        equal(
          tweetTime.getDay(),
          today.getDay(),
          "expected tweet time to be the same as today"
        ),
        equal(
          tweetTime.getHours(),
          today.getHours(),
          "expected tweet hour to match current hour"
        )
      ]);
    }
  });
});
